(function(){
    'use strict';
    var modules = require('./../modules/index.js'),
    assert = require("assert"),
    decache = require("decache");
    
    describe('app' ,function() {
        
        before(function(){
            modules.run(function(dir, m){
                decache(m);
            });
        });
        
        it('shouldWarmUpModules', function(done){
            
            var moduleList = [],
            cache,
            cacheKeys,
            allModulesCached = true;
            
            modules.run(function(dir, m){
                moduleList.push(m);
            });
  
            require("./../app.js");
            cache = require.cache;
            cacheKeys = Object.keys(cache);
            
            
            moduleList.forEach(function(m){
                allModulesCached = false;
                cacheKeys.forEach(function(key){
                    if(key.indexOf(m) !== -1){
                        if(!!cache[key].loaded) {
                            allModulesCached = true;   
                        }
                    }
                });
            });
            
            assert(allModulesCached);
            done();
            
        });
    });
    
})();