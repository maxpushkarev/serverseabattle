(function(){
'use strict';
describe('battle', function(){
    
    var decache = require("decache"),
    sinon = require("sinon"),
    util = require("util"),
    chai = require("chai"),
    areaAPI = require("area"),
    cellStates = areaAPI.cellStates,
    sandbox = sinon.sandbox.create(),
    entities,
    User,
    BattleUser,
    UserWithArea,
    sessionEventMode,
    battle,
    incrementCfg = {},
    cleanUp = function(){
        sandbox.restore();
        decache('entity');
        decache('battle');
        decache('sbdata');
        
        incrementCfg.err = false;
        delete(incrementCfg.errorHandler);
        delete(incrementCfg.handler);
    };
    
    beforeEach(function(){
        
        var NativeUserWithArea,
        NativeUser,
        sbdata;
        
        cleanUp();
        
        sbdata = require("sbdata");
        entities = require("entity");
        BattleUser = entities.BattleUser;
        
        NativeUserWithArea = entities.UserWithArea;
        NativeUser = entities.User;
        
        sandbox.stub(entities, 'User', function(mockWrapper, info) {
            var self;
            
            self = this;
            NativeUser.apply(self, [,info]);
            
            mockWrapper.mock = sandbox.mock(self);
            return self;
        });
        
        sandbox.stub(entities, 'UserWithArea', function(mockWrapper, user, area){
            var self;
            
            self = this;
            NativeUserWithArea.apply(self, [user, area]);
            
            self.parentEntity = user;
            user.childEntity = self;
            
            mockWrapper.mock = sandbox.mock(self);
            return self;
        });
        
        sandbox.stub(sbdata, 'incrementWins', function(id, handler, errorHandler){
            
            if(!!incrementCfg.err) {
                if(typeof incrementCfg.errorHandler !== 'undefined') {
                    incrementCfg.errorHandler.call(this);
                }
                errorHandler.call(this);
                return;
            }
            
            
            if(typeof incrementCfg.handler !== 'undefined') {
                incrementCfg.handler.call(this);
            }
            handler.call(this);
        });
        
        UserWithArea = entities.UserWithArea;
        User = entities.User;
        
        util.inherits(User, NativeUserWithArea);
        util.inherits(UserWithArea, NativeUserWithArea);
        
        sessionEventMode = entities.sessionEventMode;
        battle = require("battle");
    });
    
    afterEach(function(){
        cleanUp();    
    });
    
    it('removeUsersWithAreaAndCreateBattleUsers', function(done){
        
        var mockWrapperInviterWithArea = {},
        mockWrapperUserToInviteWithArea = {},
        mockWrapperInviterParent = {},
        mockWrapperUserToInviteParent = {},
        inviterArea = {},
        userToInviteArea = {},
        inviterInfo = { id : 0},
        userToInviteInfo = { id : 1},
        inviterParent = new User(mockWrapperInviterParent, inviterInfo),
        userToInviteParent = new User(mockWrapperUserToInviteParent, userToInviteInfo),
        inviterBattleUser,
        userToInviteBattleUser,
        inviter = new UserWithArea(mockWrapperInviterWithArea, inviterParent, inviterArea),
        userToInvite = new UserWithArea(mockWrapperUserToInviteWithArea, userToInviteParent, userToInviteArea);
        
        mockWrapperInviterWithArea.mock.expects('destroyEntity').once();
        mockWrapperUserToInviteWithArea.mock.expects('destroyEntity').once();
        
        mockWrapperInviterParent.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT,'onBecameIdleBattleUser', sinon.match({})).once();
        mockWrapperInviterParent.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBattleEnterWithEnemy', userToInviteInfo).once();
        mockWrapperUserToInviteParent.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBattleEnterWithEnemy', inviterInfo).once();
        mockWrapperUserToInviteParent.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT,'onBecameActiveBattleUser', sinon.match({})).once();
        
        battle.createBattle(inviter, userToInvite);
        
        chai.expect(inviterParent).to.be.instanceof(User);
        chai.expect(userToInviteParent).to.be.instanceof(User);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        chai.expect(inviterBattleUser).to.be.instanceof(BattleUser);
        chai.expect(userToInviteBattleUser).to.be.instanceof(BattleUser);
        
        chai.expect(inviterBattleUser.parentEntity).to.be.equal(inviterParent);
        chai.expect(userToInviteBattleUser.parentEntity).to.be.equal(userToInviteParent);
        
        chai.expect(inviterBattleUser.area).to.be.equal(inviterArea);
        chai.expect(userToInviteBattleUser.area).to.be.equal(userToInviteArea);
        
        chai.expect(inviterBattleUser.enemy).to.be.equal(userToInviteBattleUser);
        chai.expect(userToInviteBattleUser.enemy).to.be.equal(inviterBattleUser);
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'IDLE');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'ACTIVE');
        
        sandbox.verify();
        done();    
    });
    
    
    it('makeStepWithOverflowX', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviter = new UserWithArea({}, inviterParent, {}),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*Invalid cell coords/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 10, y: 2 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('makeStepWithNegativeX', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviter = new UserWithArea({}, inviterParent, {}),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*Invalid cell coords/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: -15, y: 2 });
        
        sandbox.verify();
        done();    
    });
    
    it('makeStepWithOverflowY', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviter = new UserWithArea({}, inviterParent, {}),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*Invalid cell coords/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 2, y: 10 });
        
        sandbox.verify();
        done();    
    });
    
    it('makeStepWithNegativeY', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviter = new UserWithArea({}, inviterParent, {}),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*Invalid cell coords/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 2, y: -15 });
        
        sandbox.verify();
        done();    
    });
    
    it('makeMultipleStepWithSameCoord', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHOT
                }]
            ]    
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*already made step/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    it('shotInIdleCellTwice', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.IDLE
                }]
            ]    
        },
        userToInviteArea = {
            rows : [
                [{
                    state : cellStates.IDLE
                }]
            ]    
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, userToInviteArea);
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        inviterBattleUser = inviterParent.childEntity;
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : false
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
            coords : {x: 0, y: 0},
            isShip : false    
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameActiveBattleUser', sinon.match({})).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameIdleBattleUser', sinon.match({})).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*already made step/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        inviterBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInFreeCellTwice', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.FREE
                }]
            ]    
        },
        userToInviteArea = {
            rows : [
                [{
                    state : cellStates.FREE
                }]
            ]    
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, userToInviteArea);
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        inviterBattleUser = inviterParent.childEntity;
        
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : false
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
            coords : {x: 0, y: 0},
            isShip : false    
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameActiveBattleUser', sinon.match({})).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameIdleBattleUser', sinon.match({})).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*already made step/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        inviterBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInShipCellTwice', function(done){
        
        var mockWrapper = {},
        inviterParent = new User({}, {}),
        userToInviteParent = new User(mockWrapper, {}),
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 2
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*already made step/) 
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInIdleCell', function(done){
        
        var inviterParent = new User({}, {}),
        userToInviteParent = new User({}, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.IDLE
                }]
            ]    
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'ACTIVE');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'IDLE');
        
        sandbox.verify();
        done();    
    });
    
    it('shotInFreeCell', function(done){
        
        var inviterParent = new User({}, {}),
        userToInviteParent = new User({}, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.FREE
                }]
            ]    
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'ACTIVE');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'IDLE');
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInNotLastShipCell', function(done){
        
        var inviterParent = new User({}, {}),
        userToInviteParent = new User({}, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 2
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'IDLE');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'ACTIVE');
        chai.expect(inviterArea).to.have.property('cellShipCount').equal(1);
        chai.expect(inviterArea.rows[0][0]).to.have.property('state').equal(cellStates.SHOT);
        
        sandbox.verify();
        done();    
    });
    
    it('shotInLastShipCell', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner', sinon.match({
            id : 123
        })).once();
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner', sinon.match({
            id : 123
        })).once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'EMPTY');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'EMPTY');
        chai.expect(inviterArea).to.have.property('cellShipCount').equal(0);
        chai.expect(inviterBattleUser).to.not.have.property('enemy');
        chai.expect(userToInviteBattleUser).to.not.have.property('enemy');
        chai.expect(inviterArea.rows[0][0]).to.have.property('state').equal(cellStates.SHOT);
        
        sandbox.verify();
        done();    
    });
    
    
    
    it('shotInLastShipCellAndDestroyInviter', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        incrementCfg.handler = function(){
            inviterBattleUser.destroyEntity();  
        };
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner').never();
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner').never();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed').once();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInLastShipCellAndDestroyInvitedUser', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        incrementCfg.handler = function(){
            userToInviteBattleUser.destroyEntity();  
        };
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner').never();
        inviterMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed').once();
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner').never();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    
    it('shotInLastShipCellWhenDatabaseFailed', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        incrementCfg.err = true;
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*save battle result/) 
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withArgs('battleFinishWithWinner').never();
        
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'serverError', sinon.match({
            textValue : sinon.match(/.*save battle result/) 
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs('battleFinishWithWinner').never();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        chai.expect(inviterBattleUser.state).to.have.property('key', 'EMPTY');
        chai.expect(userToInviteBattleUser.state).to.have.property('key', 'EMPTY');
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInLastShipCellAndDestroyInvitedUserBeforeDatabaseFailed', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        incrementCfg.err = true;
        incrementCfg.errorHandler = function(){
            userToInviteBattleUser.destroyEntity();    
        };
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed', sinon.match({ id: 123 })).once();
        
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs('serverError').never();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs('battleFinishWithWinner').never();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        chai.expect(inviterBattleUser.state).to.have.property('key', 'EMPTY');
        
        sandbox.verify();
        done();    
    });
    
    
    it('shotInLastShipCellAndDestroyInviterBeforeDatabaseFailed', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, {
            id : 123
        }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        incrementCfg.err = true;
        incrementCfg.errorHandler = function(){
            inviterBattleUser.destroyEntity();    
        };
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', sinon.match({
           coords : {x: 0, y: 0},
           isShip : true
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed').once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        userToInviteBattleUser.emit('userStepFromClient', { x: 0, y: 0 });
        
        sandbox.verify();
        done();    
    });
    
    
    it('closeBattleOnInviterDestroyed', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, { id : 123 }),
        userToInviteParent = new User(userToInviteMockWrapper, {}),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviterOnChangeStateSpy = sinon.spy(),
        userToInviteOnChangeStateSpy = sinon.spy(),
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        inviterMockWrapper.mock.expects('sendEventForParents').never();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed', sinon.match({
            id : 123
        })).once();
        
        inviterBattleUser.on('changeBattleUserState',function(name) {
            if(name === 'EMPTY') {
                inviterOnChangeStateSpy();
            }
        });
        
        userToInviteBattleUser.on('changeBattleUserState',function(name) {
            if(name === 'EMPTY') {
                userToInviteOnChangeStateSpy();
            }
        });
        
        inviterBattleUser.destroyEntity();
        
        chai.expect(inviterBattleUser).to.not.have.property('enemy');
        chai.expect(userToInviteBattleUser).to.not.have.property('enemy');
        
        sinon.assert.calledOnce(inviterOnChangeStateSpy);
        sinon.assert.calledOnce(userToInviteOnChangeStateSpy);
        
        sandbox.verify();
        done(); 
        
    });
    
    
    it('closeBattleOnUserToInviteDestroyed', function(done){
        
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviterParent = new User(inviterMockWrapper, {}),
        userToInviteParent = new User(userToInviteMockWrapper, { id : 123 }),
        inviterBattleUser,
        userToInviteBattleUser,
        inviterArea = {
            rows : [
                [{
                    state : cellStates.SHIP
                }]
            ],
            cellShipCount : 1
        },
        inviterOnChangeStateSpy = sinon.spy(),
        userToInviteOnChangeStateSpy = sinon.spy(),
        inviter = new UserWithArea({}, inviterParent, inviterArea),
        userToInvite = new UserWithArea({}, userToInviteParent, {});
        
        battle.createBattle(inviter, userToInvite);
        
        inviterBattleUser = inviterParent.childEntity;
        userToInviteBattleUser = userToInviteParent.childEntity;
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').never();
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed', sinon.match({
            id : 123
        })).once();
        
        inviterBattleUser.on('changeBattleUserState',function(name) {
            if(name === 'EMPTY') {
                inviterOnChangeStateSpy();
            }
        });
        
        userToInviteBattleUser.on('changeBattleUserState',function(name) {
            if(name === 'EMPTY') {
                userToInviteOnChangeStateSpy();
            }
        });
        
        userToInviteBattleUser.destroyEntity();
        
        chai.expect(inviterBattleUser).to.not.have.property('enemy');
        chai.expect(userToInviteBattleUser).to.not.have.property('enemy');
        
        sinon.assert.calledOnce(inviterOnChangeStateSpy);
        sinon.assert.calledOnce(userToInviteOnChangeStateSpy);
        
        sandbox.verify();
        done(); 
        
    });
    
    
});

})();