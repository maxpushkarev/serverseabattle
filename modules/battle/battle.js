(function(){
'use strict';
var entities = require("entity"),
area = require("config").area,
areaAPI = require("area"),
sbdata  = require("sbdata"),
cellStates = areaAPI.cellStates,
sideLength = area.sideLength,
maxIndex = sideLength - 1,
BattleUser = entities.BattleUser,
sessionEventMode = entities.sessionEventMode,
convertUserWithAreaIntoBattleUser = function(userWithArea){
    
    var userEntity = userWithArea.parentEntity,
    area = userWithArea.area,
    battleUser = new BattleUser(userEntity, area);
    
    userWithArea.destroyEntity();
    
    userEntity.childEntity = battleUser;
    battleUser.parentEntity = userEntity;
    
    return battleUser;
},
setBattleCreatedNotification = function(battleUser){
    battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBattleEnterWithEnemy', battleUser.enemy.parentEntity.info);
},
validateCellCoord = function(cell){
    
    if(cell.x < 0) {
        return false;
    }
    
    if(cell.x > maxIndex) {
        return false;
    }
    
    if(cell.y < 0) {
        return false;
    }
    
    if(cell.y > maxIndex) {
        return false;
    }
    
    return true;
    
},
configureStates = function(battleUser, initialStateName){
    
    var onWinnerAppeared = function(winner){
        var winnerInfo = winner.parentEntity.info;
        battleUser.emit('changeBattleUserState', 'EMPTY');
        battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'battleFinishWithWinner', winnerInfo);
        delete (battleUser.enemy);
    },
    onBattleUserDestroyed = function(){
        var myInfo = battleUser.parentEntity.info,
        enemy = battleUser.enemy;
        
        enemy.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'battleFinishOnUserDestroyed', myInfo);
        
        battleUser.emit('changeBattleUserState', 'EMPTY');
        enemy.emit('changeBattleUserState', 'EMPTY');
        
        delete (battleUser.enemy);
        delete (enemy.enemy);
    },
    states = {
        EMPTY : {
           key : 'EMPTY',
           events : []
        },
        ACTIVE : {
           key : 'ACTIVE',
           events : [{
                name : 'userStepFromClient',
                handler : function(cell){
                    
                    var area, 
                    cellInArea,
                    enemy,
                    isShip,
                    currentCellInAreaState;
                    
                    if(!validateCellCoord(cell)) {
                        battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
                            textValue : "Invalid cell coords in your step!" 
                        });
                        return;
                    }
                    
                    enemy = battleUser.enemy;
                    area = enemy.area;
                    cellInArea = area.rows[cell.x][cell.y],
                    currentCellInAreaState = cellInArea.state;
                    
                    cellInArea.state = cellStates.SHOT;
                    
                    if(currentCellInAreaState === cellStates.SHOT) {
                        battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
                            textValue : "You've already made step with this cell coords!" 
                        });
                        return;
                    }
                    
                    isShip = (currentCellInAreaState === cellStates.SHIP);
                    enemy.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'enemyShotSyncEvent', {
                        coords : {
                            x : cell.x,
                            y : cell.y
                        },
                        isShip : isShip
                    });
                    battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'selfShotSyncEvent', {
                        coords : {
                            x : cell.x,
                            y : cell.y
                        },
                        isShip : isShip
                    });
                    
                    if(isShip) {
                        
                        area.cellShipCount--;
                        
                        if(area.cellShipCount <= 0) {
                            
                            sbdata.incrementWins(battleUser.parentEntity.info.id, function(){
                                battleUser.emit('closeBattleOnWinnerAppearing', battleUser);
                                enemy.emit('closeBattleOnWinnerAppearing', battleUser);
                            }, function(){
                                battleUser.emit('saveWinsError');
                                enemy.emit('saveWinsError');
                            });
                            
                            return;    
                        }
                        
                        return;
                    }
                    
                    battleUser.emit('changeBattleUserState', 'IDLE');
                    enemy.emit('changeBattleUserState', 'ACTIVE');
                    
                    enemy.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameActiveBattleUser', {});
                    battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameIdleBattleUser', {});
                }  
            },
            {
                name : 'closeBattleOnWinnerAppearing',
                handler : function(winner){
                    onWinnerAppeared(winner);    
                }
            },
            {
                name : 'closeBattleOnBattleUserDestroyed',
                handler: onBattleUserDestroyed
            },
            {
                name : 'saveWinsError',
                handler : function(){
                    battleUser.emit('changeBattleUserState', 'EMPTY');
                    battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
                        textValue : "Can't save battle result in database!" 
                    });  
                }
            }]
        },
        IDLE : {
           key : 'IDLE',
           events : [{
                name : 'closeBattleOnWinnerAppearing',
                handler : function(winner){
                    onWinnerAppeared(winner);    
                }
            },
            {
                name : 'closeBattleOnBattleUserDestroyed',
                handler: onBattleUserDestroyed
            },
            {
                name : 'saveWinsError',
                handler : function(){
                    battleUser.emit('changeBattleUserState', 'EMPTY');
                    battleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
                        textValue : "Can't save battle result in database!" 
                    });  
                }
            }]
        }
    };
    
    battleUser.on('changeBattleUserState', function(name){
        battleUser.changeState(states[name]); 
    });
    
    battleUser.emit('changeBattleUserState', initialStateName);
    
},
configureDestroyBattleUser = function(battleUser){
    battleUser.on('destroyEntity', function(){
        battleUser.emit('closeBattleOnBattleUserDestroyed'); 
    });  
},
createBattle = function(inviter, invitedUserWithArea){
    
    var battleUserInviter = convertUserWithAreaIntoBattleUser(inviter),
    invitedBattleUser = convertUserWithAreaIntoBattleUser(invitedUserWithArea);
    
    invitedBattleUser.enemy = battleUserInviter;
    battleUserInviter.enemy = invitedBattleUser;
    
    configureStates(invitedBattleUser, 'ACTIVE');
    configureStates(battleUserInviter, 'IDLE');
    
    configureDestroyBattleUser(invitedBattleUser);
    configureDestroyBattleUser(battleUserInviter);
    
    setBattleCreatedNotification(invitedBattleUser);
    setBattleCreatedNotification(battleUserInviter);
    
    invitedBattleUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameActiveBattleUser', {});
    battleUserInviter.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameIdleBattleUser', {});
};

module.exports = {
    createBattle : createBattle  
};

})();