(function(){
'use strict';

describe('userlist', function(){

    var decache = require("decache"),
    sinon = require("sinon"),
    util = require("util"),
    chai = require("chai"),
    sandbox = sinon.sandbox.create(),
    entities,
    sessionEventMode,
    UserWithArea,
    NativeUserWithArea,
    userlist,
    cleanUp = function(){
        sandbox.restore();
        decache('entity');
        decache('userlist');
        NativeUserWithArea = undefined;
    };
    

    beforeEach(function(){
        
        var NativeUser;
        
        cleanUp();
        
        entities = require("entity");
        NativeUserWithArea = entities.UserWithArea;
        NativeUser = entities.User;
        
        sandbox.stub(entities, 'UserWithArea', function(mockWrapper, info){
            var self, user;
            
            self = this;
            NativeUserWithArea.call(self);
            
            user = new NativeUser(undefined, info);
            self.parentEntity = user;
            user.childEntity = self;
            
            mockWrapper.mock = sandbox.mock(self);
            return self;
        });
        
        UserWithArea = entities.UserWithArea;
        util.inherits(UserWithArea, NativeUserWithArea);
        sessionEventMode = entities.sessionEventMode;
        userlist = require("userlist");
        
    });
    
    afterEach(function(){
        cleanUp();    
    });
    
    it('successfullyAddedUserToList', function(done){
        var mockWrapper = {}, 
        info  = {id : 0},
        userWithArea = new UserWithArea(mockWrapper, info);
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info).once();
        mockWrapper.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        userlist.attach(userWithArea);
        sandbox.verify();
        done();
    });
    
    
    it('successfullyAddedUserToListAfterRemoving', function(done){
        var mockWrapper1 = {},
        mockWrapper2 = {},
        info  = {id : 0},
        userWithArea1 = new UserWithArea(mockWrapper1, info),
        userWithArea2 = new UserWithArea(mockWrapper2, info);
        
        
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info).once();
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaRemovedFromList', sinon.match({
            userId : '0'
        })).once();
        mockWrapper1.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info).once();
        mockWrapper2.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        userlist.attach(userWithArea1);
        userWithArea1.destroyEntity();
        userlist.attach(userWithArea2);
        
        sandbox.verify();
        done();
    });
    
    it('shareUserList', function(done){
        var mockWrapper1 = {},
        mockWrapper2 = {},
        mockWrapper3 = {},
        info1 = {id : 0},
        info2 = {id : 1},
        info3= {id : 2},
        userWithArea1 = new UserWithArea(mockWrapper1, info1),
        userWithArea2 = new UserWithArea(mockWrapper2, info2),
        userWithArea3 = new UserWithArea(mockWrapper3, info3);
        
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info1).once();
        mockWrapper1.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : [{
                info : {id : 0},
                blocked : false
            }]
        })).once();
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info2).once();
        mockWrapper2.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'serverError').never();
        
        
        mockWrapper3.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : [{
                info : {id : 0},
                blocked : false
            }, {
                info : {id : 1},
                blocked : false
            }]
        })).once();
        mockWrapper3.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info3).once();
        mockWrapper3.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'serverError').never();
        
        userlist.attach(userWithArea1);
        userlist.attach(userWithArea2);
        userlist.attach(userWithArea3);
        
        sandbox.verify();
        done();
    });
    
    
    it('errorIfMultiplyAddition', function(done){
        var mockWrapper1 = {},
        mockWrapper2 = {},
        info1 = {id : 0},
        info2 = {id : 1},
        userWithArea1 = new UserWithArea(mockWrapper1, info1),
        userWithArea2 = new UserWithArea(mockWrapper2, info2);
        
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper1.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info1).once();
        mockWrapper1.mock.expects('sendEventForParents').withArgs('serverError').never();
        
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : [{
                info : {id : 0},
                blocked : false
            }]
        })).once();
        mockWrapper2.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info2).once();
        mockWrapper2.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'serverError').once();
        
        userlist.attach(userWithArea1);
        userlist.attach(userWithArea2);
        userlist.attach(userWithArea2);
        
        sandbox.verify();
        done();
    });
    
    it('userWithAreaBornInFreeState', function(done){
        var mockWrapper = {},
        info = { id : 0},
        userWithArea = new UserWithArea(mockWrapper, info);
        
        mockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : []
        })).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info).once();
        mockWrapper.mock.expects('sendEventForParents').withArgs('unblockUserInList').never();
        
        userlist.attach(userWithArea);
        
        chai.expect(userWithArea.state).to.have.property('key').to.equal('FREE');
        sandbox.verify();
        done();
    });
    
    it('errorWhenInviteYourself', function(done){
        var mockWrapper = {},
        userWithArea = new UserWithArea(mockWrapper, { id : 0});
        userlist.attach(userWithArea);
        
        mockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.SINGLE_CLIENT, 'serverError').once();
        userWithArea.emit('requestInviteFromClient', { userId : 0 });
        
        chai.expect(userWithArea.state).to.have.property('key').to.equal('FREE');
        sandbox.verify();
        done();
    });
    
    
    it('skipWhenUserNotInList', function(done){
        var mockWrapper = {},
        userWithArea = new UserWithArea(mockWrapper, { id : 0});
        userlist.attach(userWithArea);
        
        mockWrapper.mock.expects('sendEventForParents').never();
        userWithArea.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(userWithArea.state).to.have.property('key').to.equal('FREE');
        sandbox.verify();
        done();
    });
    
    it('inviteAnotherUser', function(done){
        var inviterMockWrapper = {},
        userToInviteMockWrapper = {},
        inviter = new UserWithArea(inviterMockWrapper, { id : 0}),
        userToInvite = new UserWithArea(userToInviteMockWrapper, { id : 1});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameInviter', sinon.match({})).once();
        inviterMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'blockUserInList', sinon.match({
            userId : '0'
        })).once();
        
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'onBecameInvited', sinon.match({
            id : 0
        })).once();
        userToInviteMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'blockUserInList', sinon.match({
            userId : '1'
        })).once();
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(userToInvite);
        chai.expect(userToInvite.inviter).to.equal(inviter);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
        
        sandbox.verify();
        done();
    });
    
    
    it('skipWhenUserForInviteAlreadyInvitedBySomeone', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        third = new UserWithArea({}, {id : 2});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        userlist.attach(third);
        
        
        third.emit('requestInviteFromClient', { userId : 1 });
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        chai.expect(userToInvite.inviter).to.equal(third);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('FREE');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
        chai.expect(third.state).to.have.property('key').to.equal('INVITER');
        
        sandbox.verify();
        done();
    });
    
    it('skipWhenUserForInviteAlreadyInvitedSomeone', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        third = new UserWithArea({}, {id : 2});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        userlist.attach(third);
        
        
        inviter.emit('requestInviteFromClient', { userId : 2 });
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(third);
        chai.expect(userToInvite.inviter).to.be.undefined;
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('FREE');
        chai.expect(third.state).to.have.property('key').to.equal('INVITED');
        
        sandbox.verify();
        done();
    });
    
    
    it('skipWhenUserForInviteInvitedAnotherOneByHimself', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        third = new UserWithArea({}, {id : 2});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        userlist.attach(third);
        
        
        userToInvite.emit('requestInviteFromClient', { userId : 2 });
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        
        chai.expect(inviter.state).to.have.property('key').to.equal('FREE');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITER');
        
        sandbox.verify();
        done();
    });
    
    
    it('skipWhenInviterAlreadyInvited', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        third = new UserWithArea({}, {id : 2});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        userlist.attach(third);
        
        
        third.emit('requestInviteFromClient', { userId : 0 });
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        chai.expect(inviter.inviter).to.be.equal(third);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITED');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('FREE');
        
        sandbox.verify();
        done();
    });
    
    it('setInviterFreeAfterInvitedUserDestroyed', function(done){
        var mockWrapper = {}, 
        inviter = new UserWithArea(mockWrapper, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        emptyStateSpy = sinon.spy();
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(userToInvite);
        chai.expect(userToInvite.inviter).to.equal(inviter);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
        
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'invitedUserLeftTheGame', sinon.match({
            id : 1
        })).once();
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'unblockUserInList', sinon.match({
            userId : '0'
        })).once();
        
        userToInvite.on('changeStateEvent', function(key){
            if(key === 'EMPTY') {
                emptyStateSpy();
            } 
        });
        
        userToInvite.destroyEntity();
        userToInvite.destroyEntity();
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        chai.expect(userToInvite.inviter).to.be.undefined;
        
        chai.expect(inviter.state).to.have.property('key').to.equal('FREE');
        chai.expect(userToInvite.state).to.be.undefined;
        
        sandbox.verify();
        sinon.assert.calledOnce(emptyStateSpy);
        done();
    });
    
    
    it('setInvitedUserFreeAfterInviterDestroyed', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        mockWrapper = {},
        userToInvite = new UserWithArea(mockWrapper, { id : 1}),
        emptyStateSpy = sinon.spy();
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(userToInvite);
        chai.expect(userToInvite.inviter).to.equal(inviter);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
        
        inviter.on('changeStateEvent', function(key){
            if(key === 'EMPTY') {
                emptyStateSpy();
            } 
        });
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'inviterLeftTheGame', sinon.match({
            id : 0
        })).once();
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'unblockUserInList', sinon.match({
            userId : '1'
        })).once();
        
        inviter.destroyEntity();
        inviter.destroyEntity();
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        chai.expect(userToInvite.inviter).to.be.undefined;
        
        chai.expect(userToInvite.state).to.have.property('key').to.equal('FREE');
        chai.expect(inviter.state).to.be.undefined;
        
        sandbox.verify();
        sinon.assert.calledOnce(emptyStateSpy);
        done();
    });
    
    
    it('rejectInviteFromAnotherUser', function(done){
        var mockWrapper = {},
        inviter = new UserWithArea(mockWrapper, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(userToInvite);
        chai.expect(userToInvite.inviter).to.equal(inviter);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
        
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.BROADCAST_CLIENTS, 'unblockUserInList', sinon.match({ userId : '0'})).once();
        mockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'youAreRejected', sinon.match({ id : 1})).once();
        
        userToInvite.emit('rejectInviteFromClient');
        
        chai.expect(inviter.userToInvite).to.be.undefined;
        chai.expect(userToInvite.inviter).to.be.undefined;
        chai.expect(inviter.state).to.have.property('key').to.equal('FREE');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('FREE');
        
        sandbox.verify();
        done();
    });
    
    
    it('acceptInviteFromAnotherUser', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        battleFake = {
            handler : function(){}    
        },
        battleSpy = sinon.spy(battleFake, 'handler'),
        userToInvite = new UserWithArea({}, { id : 1});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        chai.expect(inviter.userToInvite).to.equal(userToInvite);
        chai.expect(userToInvite.inviter).to.equal(inviter);
        
        chai.expect(inviter.state).to.have.property('key').to.equal('INVITER');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('INVITED');
       
        userToInvite.on('createBattleWithInviter', function(inviter){
            battleFake.handler(inviter);    
        });
        
        userToInvite.emit('acceptInviteFromClient');
        
        chai.expect(inviter.state).to.have.property('key').to.equal('EMPTY');
        chai.expect(userToInvite.state).to.have.property('key').to.equal('EMPTY');

        
        sandbox.verify();
        sinon.assert.calledWithExactly(battleSpy, inviter);
        sinon.assert.calledOnce(battleSpy);
        
        done();
    });
    
    it('shareBlockedUsersInList', function(done){
        var inviter = new UserWithArea({}, { id : 0}),
        userToInvite = new UserWithArea({}, { id : 1}),
        thirdMockWrapper = {},
        fourthMockWrapper = {},
        third = new UserWithArea(thirdMockWrapper, {id : 2}),
        fourth = new UserWithArea(fourthMockWrapper, {id : 3});
        
        userlist.attach(inviter);
        userlist.attach(userToInvite);
        
        inviter.emit('requestInviteFromClient', { userId : 1 });
        
        thirdMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : [{
                info : {id : 0},
                blocked : true
            }, {
                info : {id : 1},
                blocked : true
            }]
        })).once();
        thirdMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList').once();
        
        userlist.attach(third);
        
        fourthMockWrapper.mock.expects('sendEventForParents').withExactArgs(sessionEventMode.SINGLE_CLIENT, 'currentUserList', sinon.match({
            users : [{
                info : {id : 0},
                blocked : true
            }, {
                info : {id : 1},
                blocked : true
            }, {
                info : {id : 2},
                blocked : false
            }]
        })).once();
        fourthMockWrapper.mock.expects('sendEventForParents').withArgs(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList').once();
        
        userlist.attach(fourth);
        
        sandbox.verify();
        done();
    });
    
    
});

})();