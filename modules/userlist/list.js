(function(){
'use strict';
var list = {},
entities = require("entity"),
sessionEventMode = entities.sessionEventMode,
UserWithArea = entities.UserWithArea,
shareUserList = function(userWithArea){
    
    var prop,
    currentUserWithArea,
    output = { users : []},
    users = output.users;
    
    for(prop in list) {
        
        if(!list.hasOwnProperty(prop)) {
            continue;
        }
        
        currentUserWithArea = list[prop];
        if(currentUserWithArea === userWithArea) {
            continue;
        }
        
        users.push({
            info : currentUserWithArea.parentEntity.info,
            blocked : (currentUserWithArea.state.key !== 'FREE')
        });
    }
    
    userWithArea.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'currentUserList', output);
},
attach = function(thisUserWithArea){
    
    var user = thisUserWithArea.parentEntity,
    info = user.info,
    id = info.id,
    states;
    
    if(list[id] instanceof UserWithArea) {
        delete list[id];
        thisUserWithArea.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
            textValue : 'User <'+id+'> with area already exists!' 
        });
        return;
    }
    
    states = {
        FREE : {
            key : 'FREE',
            events : [{
                name : 'requestInviteFromClient',
                handler : function(data){
                    
                    var id = data.userId,
                    userToInvite = list[id];
                    
                    if(typeof(userToInvite) === 'undefined') {
                        return;
                    }
                    
                    if(userToInvite === thisUserWithArea) {
                        thisUserWithArea.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'serverError', {
                            textValue : "You can't invite yourself!" 
                        });
                        return;
                    }
                    
                    userToInvite.emit('invitedByUser', thisUserWithArea);
                }  
            },
            {
                name : 'invitedByUser',
                handler : function(inviter){
                    
                    inviter.userToInvite = thisUserWithArea;
                    thisUserWithArea.inviter = inviter;
                    
                    inviter.emit('changeStateEvent', 'INVITER');
                    thisUserWithArea.emit('changeStateEvent', 'INVITED');
                    
                    thisUserWithArea.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'blockUserInList', { 
                        userId : id.toString()
                    });
                    inviter.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'blockUserInList', { 
                        userId : inviter.parentEntity.info.id.toString()
                    });
                    
                    inviter.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameInviter', {});
                    thisUserWithArea.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'onBecameInvited', inviter.parentEntity.info);
                }
            }]   
        },
        INVITED : {
            key : 'INVITED',
            events : [{
                name : 'resetUserAreaStateOnDestroy',
                handler : function(){
                    var inviter = thisUserWithArea.inviter;
                    delete(thisUserWithArea.inviter);
                    
                    inviter.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'invitedUserLeftTheGame', info);
                    inviter.emit('restoreToFree');
                }
            },
            {
                name : 'restoreToFree',
                handler : function(){
                    delete(thisUserWithArea.inviter);
                    thisUserWithArea.emit('changeStateEvent', 'FREE');
                    thisUserWithArea.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'unblockUserInList', { 
                        userId : id.toString()
                    });
                }
            },
            {
                name : 'acceptInviteFromClient',
                handler : function(){
                    var inviter = thisUserWithArea.inviter;
                    inviter.emit('changeStateEvent', 'EMPTY');
                    thisUserWithArea.emit('changeStateEvent', 'EMPTY');
                    
                    thisUserWithArea.sendEventForParents(sessionEventMode.SERVER_ONLY, 'createBattleWithInviter', inviter);
                }
            },
            {
                name : 'rejectInviteFromClient',
                handler : function(){
                    var inviter = thisUserWithArea.inviter;
                    inviter.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'youAreRejected', info);
                    thisUserWithArea.emit('restoreToFree');
                    inviter.emit('restoreToFree');
                }
            }]    
        },
        INVITER : {
            key : 'INVITER',
            events : [{
                name : 'resetUserAreaStateOnDestroy',
                handler : function(){
                    var invitedUser = thisUserWithArea.userToInvite;
                    delete(thisUserWithArea.userToInvite);
                    
                    invitedUser.sendEventForParents(sessionEventMode.SINGLE_CLIENT, 'inviterLeftTheGame', info);
                    invitedUser.emit('restoreToFree');    
                }    
            },
            {
                name : 'restoreToFree',
                handler : function(){
                    delete(thisUserWithArea.userToInvite);
                    thisUserWithArea.emit('changeStateEvent', 'FREE');
                    thisUserWithArea.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'unblockUserInList', { 
                        userId : id.toString()
                    });
                }    
            }]    
        },
        EMPTY : {
           key : 'EMPTY',
           events : []
        }
    };
    
    
    thisUserWithArea.on('changeStateEvent', function(stateName) {
        thisUserWithArea.changeState(states[stateName]);    
    });
    
    thisUserWithArea.emit('changeStateEvent', 'FREE');
    
    thisUserWithArea.on('destroyEntity', function(){
        
        delete list[id];
        thisUserWithArea.emit('resetUserAreaStateOnDestroy');
        thisUserWithArea.emit('changeStateEvent', 'EMPTY');
        
        thisUserWithArea.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaRemovedFromList', {
            userId : id.toString()
        });
    });
    
    list[id] = thisUserWithArea;

    thisUserWithArea.sendEventForParents(sessionEventMode.BROADCAST_CLIENTS, 'userWithAreaAddedInList', info);
    shareUserList(thisUserWithArea);
};

module.exports = {
    attach : attach
};

})();