(function(){
'use strict';
var sio = require('socket.io-client'),
sinon = require("sinon"),
assert = require("assert"),
sinAssert = sinon.assert,
decache = require("decache"),
options ={
  transports: ['websocket'],
  'force new connection': true
},
url;

    
    describe('network' ,function() {
        
        var entities,
        createSessionSpy,
        destroySessionEventSpy,
        sessionReadyEventSpy,
        playerSpy,
        sandbox = sinon.sandbox.create(),
        onDisconnect,
        cleanUp = function(){
            sandbox.restore();
            decache('network');
            decache('player');
            decache('entity');
            decache('socket-io');
            
            url = 'http://'+process.env.IP+':'+process.env.PORT;
            
            onDisconnect = undefined;   
        };
        
        before(cleanUp);
        
        afterEach(cleanUp);
        
        beforeEach(function(){
            
            var socketio,
            socketioListen,
            slice = Array.prototype.slice,
            Entity,
            player = require("player");
            
            entities = require("entity");
            Entity = entities.Entity;
            createSessionSpy = sinon.spy();
            destroySessionEventSpy = sinon.spy();
            sessionReadyEventSpy = sinon.spy();
            playerSpy = sinon.spy();
            
            sandbox.stub(player, 'prepare', function() {
                playerSpy();   
            });
            
            sandbox.stub(entities, 'Session', function(){
                var fakeSession = new Entity();
                createSessionSpy();
                fakeSession.on('destroyEntity', destroySessionEventSpy);
                
                fakeSession.sendClientEvent = function(){
                    sessionReadyEventSpy();
                };
                
                return fakeSession;
            });
            
            socketio = require('socket.io');
            socketioListen = socketio.listen;
            sandbox.stub(socketio, 'listen', function(){
                var self = this,
                socketObj = socketioListen.apply(self, slice.call(arguments)),
                sockets = socketObj.sockets,
                socketsEmit = sockets.emit;
                
                sandbox.stub(socketObj.sockets, 'emit', function() {
                    var args = slice.call(arguments),
                    output = socketsEmit.apply(this, args),
                    socket;
                    
                    if(args[0] === 'connection'){
                        socket = args[1];
                        sandbox.stub(socket, 'emit').withArgs('disconnect').returns((function(){
                            
                            var listeners = socket.listeners('disconnect'),
                            listenersCount = listeners.length,
                            i;
                            
                            for(i = 0 ; i< listenersCount; i++)
                            {
                                listeners[i].call(socket);
                            }
                            
                            if(typeof onDisconnect === 'undefined'){
                                return;
                            }
                            
                            onDisconnect(socket);

                        })());
                    }
                    
                    return output;
                });
                
                return socketObj;
            });
            
            require('network');
            
        });
        
        
        it('createAndRemoveSession', function(done){
            
            var client = sio.connect(url, options);
            
            onDisconnect = function(){
                
                sinAssert.calledOnce(destroySessionEventSpy);
                sinAssert.calledOnce(sessionReadyEventSpy);
                sinAssert.calledOnce(playerSpy);
                
                assert(destroySessionEventSpy.calledAfter(sessionReadyEventSpy));
                assert(sessionReadyEventSpy.calledAfter(playerSpy));
              
                done();
            };
            
            client.once("connect", function () {
                sinAssert.calledOnce(createSessionSpy);
                client.disconnect();
            });
            
                
        });
        
    });
    
})();