(function initServerNetwork(){
    'use strict';
    var express = require('express'),
    app = express(),
    server  = require('http').createServer(app),
    player = require("player"),
    port = process.env.PORT,
    sio = require('socket.io').listen(server),
    Session = require("entity").Session,
    clientSessionLifecycleHandler = function(socket){
        
        var session = new Session(socket);
        socket.on('disconnect', function(){
            session.destroyEntity();
            session = undefined;
        }); 
        
        player.prepare(session);
        session.sendClientEvent('sessionReady');
    };
    
    sio.sockets.on('connection', clientSessionLifecycleHandler);
    
    server.listen(port, function(){
        console.log('Listen to port '+ port);    
    });
    
    module.exports = {
        close : function(){
            server.close();
        }  
    };
    
})();