(function(){
    'use strict';
    var fs = require('fs'),
    path = require("path"),
    childs,
    childsLength = 0,
    child,
    index;
    
    module.exports = {
        
        run : function(callbackIterator, callbackReady){
            
            childs = fs.readdirSync('./modules');
            childsLength = childs.length;
            
            for(index = 0; index < childsLength; index++){
                
                child = childs[index];
                
                if(!fs.lstatSync(path.join('./modules', child)).isDirectory())
                {
                    continue;
                }
                
                callbackIterator.apply(this, ['./modules', child]);
            }  
    
            if(typeof callbackReady !== 'undefined')
            {
                callbackReady.call(this);
            }
    
        }
        
    };
    

    
})();