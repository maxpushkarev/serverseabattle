(function(){
'use strict';
var mongoose = require('mongoose'),
connectedToSB = false,
cfg = require("config"),
dbCfg = cfg.database,
connectionString = 'mongodb://'+dbCfg.user+':'+dbCfg.password+'@db-serverseabattle-2710.nodechef.com:5381/serverseabattle',
db = mongoose.connection,
userSchema = mongoose.Schema({
    fbID : Number,
    wins : Number
}),
User = mongoose.model('User', userSchema);

db.on('error', console.error.bind(console, 'Database connection error: '));

db.once('open', function() {
  connectedToSB = true;
  console.log('Opened database connection');
});

db.once('close', function() {
  connectedToSB = false;
  console.log('Lost database connection');
});

mongoose.connect(connectionString);

module.exports = {
    getUser : function(id, handler, errorHandler){
        
        if(!connectedToSB) {
            errorHandler.call(this);
            return;
        }
        
        User.findOneAndUpdate({
            fbID: id    
        },{
            $setOnInsert: { fbID: id, wins : 0 }
        },{
            'new': true,
            'upsert': true
        }, function(err, user){
            
            if(!!err){
                errorHandler.call(this, err);
                return;
            }
            
            handler.call(this, user);
        });
    },
    incrementWins : function(id, handler, errorHandler){

        if(!connectedToSB) {
            errorHandler.call(this);
            return;
        }

        User.findOneAndUpdate({
            fbID: id    
        },{
            $inc: { wins: 1 }
        },{}, function(err, user){
            
            if(!!err){
                errorHandler.call(this, err);
                return;
            }
            
            handler.call(this, user);
        });
        
    }
};
})();