(function(){
    'use strict';
    describe('sbdata', function(){
       
        var decache = require("decache"),
        EventEmitter = require("events").EventEmitter,
        sinon = require("sinon"),
        sandbox = sinon.sandbox.create(),
        mongoose,
        cleanUp = function() {
            sandbox.restore();
            decache('mongoose');
            decache('sbdata');
        },
        prepareMongoose = function(cfg, needToOpen){
            
            mongoose = require("mongoose");
            mongoose.connection = new EventEmitter();
            
            sandbox.stub(mongoose, 'model', function(){
                
                var Model = function(obj){
                    this.fbID = obj.fbID;
                    this.wins = obj.wins;
                };
                
                Model.findOneAndUpdate = function(query, update, options, handler){
                    handler.apply(this, [cfg.find.err, cfg.find.user]);    
                };
                
                return Model;
                
            });
            
            sandbox.stub(mongoose, 'connect', function(){
                if(!!needToOpen) {
                    mongoose.connection.emit('open');    
                }
            });

            mongoose.closeTestConnection = function(){
                mongoose.connection.emit('close');  
            };
            
            return require("sbdata");
        };
        
        beforeEach(function(){
            cleanUp(); 
        });
        
        afterEach(function(){
            cleanUp(); 
        });
        
        
        it('errorWhenGetUserWithoutConnectionToDatabase', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, false);
            
            sbdata.getUser(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
        
        
        it('errorWhenGetUserAfterLostConnection', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, true);
            
            mongoose.closeTestConnection();
            
            sbdata.getUser(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
        
        
        
        it('errorWhenGetUserOnConnectionToDatabase', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : true,
                    'user' : user
                }
            }, true);
            
            sbdata.getUser(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
        
        it('getValidUser', function(done){
        
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, true);
            
            sbdata.getUser(0, successSpy, errSpy);
            
            sinon.assert.neverCalledWith(errSpy);
            sinon.assert.calledOnce(successSpy);
            successSpy.calledWith(user);
            
            done();
            
        });
    
        it('successfullyIncrementWins', function(done){
        
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, true);
            
            sbdata.incrementWins(0, successSpy, errSpy);
            
            sinon.assert.neverCalledWith(errSpy);
            sinon.assert.calledOnce(successSpy);
            successSpy.calledWith(user);
            
            done();
            
        });
    
    
        it('errorWhenIncrementWinsWithoutConnectionToDatabase', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, false);
            
            sbdata.incrementWins(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
        
        
        it('errorWhenIncrementWinsAfterLostConnection', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : false,
                    'user' : user
                }
            }, true);
            
            mongoose.closeTestConnection();
            
            sbdata.incrementWins(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
        
        
        
        it('errorWhenIncrementWinsOnConnectionToDatabase', function(done){
            
            var user = { fbID:0, wins:123},
            errSpy = sinon.spy(),
            successSpy = sinon.spy(),
            sbdata = prepareMongoose({
                'find' : {
                    'err' : true,
                    'user' : user
                }
            }, true);
            
            sbdata.incrementWins(0, successSpy, errSpy);
            sinon.assert.calledOnce(errSpy);
            sinon.assert.neverCalledWith(successSpy);
            
            done();
            
        });
    
    
    });
    
})();