(function(){
'use strict';
var util = require("util"),
EventEmitter = require("events").EventEmitter,
slice = Array.prototype.slice,
sessionEventMode = {
    SERVER_ONLY : {},
    SINGLE_CLIENT : {},
    BROADCAST_CLIENTS : {},
    ALL_CLIENTS : {}
},
validateArg = function(arg){
    
    if(arg === sessionEventMode.SERVER_ONLY) {
        return true;
    }
    
    if(arg === sessionEventMode.SINGLE_CLIENT) {
        return true;
    }
    
    if(arg === sessionEventMode.BROADCAST_CLIENTS) {
        return true;
    }
    
    if(arg === sessionEventMode.ALL_CLIENTS) {
        return true;
    }
    
    return false;
},
emitParentEventOnEntity = function(entity, args){
    
    var inputArgs = args.slice(0),
    length = inputArgs.length,
    evt,
    firstArg;
    
    if(length === 0) {
        return;
    }
    
    firstArg = inputArgs[0];
    
    if(validateArg(firstArg)) {
        inputArgs.splice(0, 1);
    }
    
    if(inputArgs.length === 0) {
        return;
    }
    
    evt = inputArgs[0];
    if(!!entity.lockEvents[evt]) {
        return;
    }
    
    entity.lockEvents[evt] = true;
    entity.emit.apply(entity, inputArgs);
    delete (entity.lockEvents[evt]);
},
Entity = function(){
    
    var self = this,
    clearState;
    
    EventEmitter.call(self);
    self.lockEvents = {};
    
    clearState = function(){
        var currentState = self.state;
        if(typeof currentState !== 'undefined') {
            currentState.events.forEach(function(evt){
                self.removeAllListeners(evt.name);
            });  
        }    
    };
    
	self.changeState = function(state){
        
        clearState();
        
        self.state = state;
        state.events.forEach(function(evt){
            self.on(evt.name, evt.handler);
        });
            
    };
	
    self.sendEventForChilds = function(){
        
        var args = slice.call(arguments),
        childEntity = self.childEntity,
        evt = args[0];
        
        if(childEntity instanceof Entity) {
            childEntity.sendEventForChilds.apply(childEntity, args);
        }
        
        if(!!self.lockEvents[evt]) {
            return;
        }
        
        self.lockEvents[evt] = true;
        self.emit.apply(self, args);
        delete (self.lockEvents[evt]);
    };
    
    self.sendEventForParents = function(){
        var args = slice.call(arguments),
        parentEntity = self.parentEntity;
        
        if(parentEntity instanceof Entity) {
            parentEntity.sendEventForParents.apply(parentEntity, args);
        }
        
        emitParentEventOnEntity(self, args);
    };
    
    self.destroyEntity = function(){
        self.sendEventForChilds('destroyEntity');
        self.sendEventForChilds('destroyEntityConnections');
        self.removeAllListeners('destroyEntity');
        self.removeAllListeners('destroyEntityConnections');
    };
    
    self.on('destroyEntityConnections', function(){
        clearState();
        delete(self.state);
        delete(self.childEntity);
        delete(self.parentEntity);
    });
    
},
UserWithArea = function(user, area){
    
    var self = this;
    Entity.call(self);
    
    self.area = area;
    self.on('destroyEntity', function(){
        user = undefined;
        area = undefined;
        delete self.area;
    });
},
BattleUser = function(user, area) {
    
    var self = this;
    UserWithArea.apply(self, [user, area]);
    
    self.on('destroyEntity', function(){
        area = undefined;
        user = undefined;
    });
},
User = function(session, info){
    
    var self = this;
    Entity.call(self);

    self.info = info;
    self.on('destroyEntity', function(){
        
        session = undefined;
        info = undefined;
        
        delete self.info;
        delete self.db;
    });
    
},
protocol = require("protocol"),
protocolEvents = protocol.events,
Session = function(socket){
    
    var self = this,
    states = {
        ACTIVE_SESSION : {
            events : [{
                name : 'sendClientEvent',
                handler : function(evt, data){
                    var encodedData = protocol.encode(evt, data); 
                    self.socket.emit(evt, encodedData);    
                }    
            },
            {
                name : 'sendBroadcastClientEvent',
                handler : function(evt, data){
                    var encodedData = protocol.encode(evt, data);
                    self.socket.broadcast.emit(evt, encodedData);
                }    
            },
            {
                name : 'sendEventForAllClients',
                handler : function(evt, data){
                    var encodedData = protocol.encode(evt, data);
                    self.socket.emit(evt, encodedData);
                    self.socket.broadcast.emit(evt, encodedData);  
                }    
            }]    
        },
    },
    _emit = function(inputArgs){
        var evt = inputArgs[0];
        if(!!self.lockEvents[evt]) {
            return;
        }
        
        self.lockEvents[evt] = true;
        self.emit.apply(self, inputArgs);
        delete (self.lockEvents[evt]);
    };
    
    Entity.call(self);
    self.socket = socket;
    
    self.on('destroyEntityConnections', function(){
        self.socket.disconnect();
        delete (self.socket);
        socket = undefined;
        states = undefined;
    });
    
    self.changeState(states.ACTIVE_SESSION);
    
    self.sendClientEvent = function(evt, data){
        self.emit('sendClientEvent', evt, data);
    };
    
    self.sendBroadcastClientEvent = function(evt, data){
        self.emit('sendBroadcastClientEvent', evt, data);
    };
    
    self.sendEventForAllClients = function(evt, data){
        self.emit('sendEventForAllClients', evt, data);
    };

    self.sendEventForParents = function(){
        
        var args = slice.call(arguments),
        inputArgs = args.slice(0),
        length = args.length,
        mode;
        
        if(length === 0) {
            return;
        }
        
        mode = args[0];
        args.splice(0, 1);
        
        if(args.length === 0) {
            return;
        }
        
        if(mode === sessionEventMode.SERVER_ONLY) {
            _emit(args);
            return;
        }
        
        if(mode === sessionEventMode.SINGLE_CLIENT) {
            _emit(args);
            self.sendClientEvent.apply(self, args);
            return;
        }
        
        if(mode === sessionEventMode.BROADCAST_CLIENTS) {
            _emit(args);
            self.sendBroadcastClientEvent.apply(self, args);
            return;
        }
        
        if(mode === sessionEventMode.ALL_CLIENTS) {
            _emit(args);
            self.sendEventForAllClients.apply(self, args);
            return;
        }
        
        _emit(inputArgs);
    },

    protocolEvents.forEach(function(evt){
       self.socket.on(evt, function(data){
           var decodedData,
           errMsg;
           
            try
            {
                decodedData = protocol.decode(evt, data);
                self.sendEventForChilds(evt, decodedData);
            }
            catch(err) {
                errMsg = err.message;
                errMsg = ((typeof errMsg === 'string' || errMsg instanceof String) && !!errMsg) ? errMsg : 'Unknown error';
                self.sendClientEvent('serverError', {
                    textValue : "Server error on <"+evt+"> : "+ errMsg
                });
            }
            
       });
    });
};

util.inherits(Entity, EventEmitter);
util.inherits(Session, Entity);
util.inherits(User, Entity);
util.inherits(UserWithArea, Entity);
util.inherits(BattleUser, UserWithArea);

module.exports = {
    Entity : Entity,
    Session : Session,
    User : User,
    UserWithArea : UserWithArea,
    BattleUser : BattleUser,
    sessionEventMode : sessionEventMode
};

})();