(function(){
'use strict';

describe('session', function(){
    
    var cfg,
    timeout = 567,
    sio = require('socket.io-client'),
    options ={
        transports: ['websocket'],
        'force new connection': true
    },
    EventEmitter = require("events").EventEmitter,
    decache = require("decache"),
    sinon = require("sinon"),
    assert = require("assert"),
    chai =require("chai"),
    protocol,
    port,
    url,
    entities,
    client1,
    client2,
    sandbox = sinon.sandbox.create(),
    NativeSession,
    prepareSession,
    prepareClients;
    
    beforeEach(function(){
            
        decache('config');
        decache('protocol');
        decache('entity');
        decache("network");
            
        cfg = require("config");
        cfg.messages = {
            "client_Ping" : {
                "messageName" : "Ping",
                "protoFile" : ['./../entity/test', 'ping.proto']
            },
            "server_Pong" : {
                "messageName" : "Pong",
                "protoFile" : ['./../entity/test', 'pong.proto']
            },
            "sessionReady" : {
                "messageName" : "Empty",
                "protoFile" : ['./../protocol/protofiles', 'Empty.proto']
            },
            "serverError" : {
                "messageName" : "Message",
                "protoFile" : ['./../protocol/protofiles', 'Message.proto']
            }
        };
        
        protocol = require("protocol");
        entities = require("entity");
        NativeSession = entities.Session;
        port = process.env.PORT,
        url = 'http://'+process.env.IP+':'+port;
        
        prepareSession = function(onPing, user){
            sandbox.stub(entities, 'Session', function(socket){
                
                var output = new NativeSession(socket);
                output.on('client_Ping', function(message){
                    onPing(message, output);
                });
                
                output.childEntity = user;
                
                return output;
            });
            
            require("network");
        };
        
        prepareClients = function(onClientConnected, timeoutCheck){
           
            client1 = sio.connect(url, options);
            client1.once("connect", function () {
            
                client2 = sio.connect(url, options);
                client2.once("connect", function () {
                    onClientConnected(client1, client2);
                });
            
            });
            
            setTimeout(function(){
            
                client1.disconnect();
                client2.disconnect();
                
                timeoutCheck();
            
            }, timeout);
            
        };
        
    });
    
    afterEach(function(){
        sandbox.restore();
        
        decache('config');
        decache('protocol');
        decache('entity');
        require("network").close();
        decache("network");
    });
    
    it('receiveServerEventWithValidUser', function(done){
        
        var sessionSpy = sinon.spy(),
        userSpy = sinon.spy(),
        user = new entities.User({});
    
        user.on('client_Ping', userSpy);
        prepareSession(function(message, session){
            sessionSpy();    
        }, user);
        
        prepareClients(function(client1, client2){
            
            client1.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
                
        }, function(){
            sinon.assert.calledOnce(sessionSpy);
            sinon.assert.calledOnce(userSpy);
            done();    
        });

    });
    
    
    it('receiveServerEventWithInvalidUser', function(done){
        
        var sessionSpy = sinon.spy(),
        userSpy = sinon.spy(),
        user = new EventEmitter();
    
        user.on('client_Ping', userSpy);
        prepareSession(function(message, session){
            sessionSpy();    
        }, user);
        
        prepareClients(function(client1, client2){
            
            client1.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
                
        }, function(){
            sinon.assert.calledOnce(sessionSpy);
            sinon.assert.neverCalledWith(userSpy);
            done();    
        });

    });
    
    
    it('sendClientEvent', function(done){
        
        var spy1 = sinon.spy(),
        spy2 = sinon.spy(),
        sessionForPong;
        
        prepareSession(function(message, session){
            
            if(typeof sessionForPong !== 'undefined'){
                return;
            }
            
            sessionForPong = session;
            assert.equal(message.text, 'CLIENT_PING');
            session.sendClientEvent('server_Pong', {
                text : 'SERVER_PONG'
            });
            
        });
        
        prepareClients(function(client1, client2){
            
            client1.on('server_Pong', function(message){
                assert.equal(protocol.decode('server_Pong', message).text, 'SERVER_PONG');
                spy1();
            });
            client2.on('server_Pong', function(message){
                assert.equal(protocol.decode('server_Pong', message).text, 'SERVER_PONG');
                spy2();
            });
            
            
            client1.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
            client2.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
                
        }, function(){
            sinon.assert.calledOnce(spy1);
            sinon.assert.neverCalledWith(spy2);
            done();    
        });

    });
    
    
    it('sendIncorrectClientEvent', function(done){
        
        var errSpy = sinon.spy(),
        errMsg;
        
        prepareSession(function(message, session){
        });
        
        prepareClients(function(client1, client2){
            client1.emit('client_Ping', {});
            client1.on('serverError', function(message){
                errMsg = protocol.decode('serverError', message);
                errSpy();    
            });
        }, function(){
            sinon.assert.calledOnce(errSpy);
            chai.expect(errMsg).to.have.property('textValue').to.match(/.+Illegal buffer/);
            done();    
        });

    });
    
    it('sendBroadcastClientEvent', function(done){
        
        var spy1 = sinon.spy(),
        spy2 = sinon.spy(),
        sessionForPong;
        
        prepareSession(function(message, session){
            
            if(typeof sessionForPong !== 'undefined'){
                return;
            }
            
            sessionForPong = session;
            session.sendBroadcastClientEvent('server_Pong', {
                text : 'SERVER_BROADCAST_PONG'
            });
            
        });
        
        prepareClients(function(client1, client2){
            
            client1.on('server_Pong', function(message){
                spy1();
            });
            client2.on('server_Pong', function(message){
                spy2();
            });
            
            
            client1.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
                
        }, function(){
            sinon.assert.calledOnce(spy2);
            sinon.assert.neverCalledWith(spy1);
            done();    
        });

    });
    
    
    it('sendEventForAllClients', function(done){
        
        var spy1 = sinon.spy(),
        spy2 = sinon.spy(),
        sessionForPong;
        
        prepareSession(function(message, session){
            
            if(typeof sessionForPong !== 'undefined'){
                return;
            }
            
            sessionForPong = session;
            session.sendEventForAllClients('server_Pong', {
                text : 'SERVER_BROADCAST_PONG'
            });
            
        });
        
        prepareClients(function(client1, client2){
            
            client1.on('server_Pong', function(message){
                spy1();
            });
            client2.on('server_Pong', function(message){
                spy2();
            });
            
            
            client1.emit('client_Ping', protocol.encode('client_Ping', {
                text : 'CLIENT_PING'
            }));
                
        }, function(){
            sinon.assert.calledOnce(spy2);
            sinon.assert.calledOnce(spy1);
            done();    
        });
    
    });
    
    
});

})();