(function(){
'use strict';
var decache = require("decache"),
sinon = require("sinon"),
chai = require("chai"),
sandbox = sinon.sandbox.create(),
entityAPI,
Session,
Entity,
User,
UserWithArea,
BattleUser,
EventEmitter = require("events").EventEmitter,
sessionMode,
assert = require("assert");

describe('entities', function(){
   
    beforeEach(function(){
        
        decache('entity');
        
        entityAPI = require("entity");
        Session = entityAPI.Session;
        Entity = entityAPI.Entity;
        User = entityAPI.User;
        UserWithArea = entityAPI.UserWithArea,
        BattleUser = entityAPI.BattleUser,
        sessionMode = entityAPI.sessionEventMode;
    });
    
    afterEach(function(){
        sandbox.restore();
        decache('entity');
    });
   
    it('entityInheritedFromEventEmitter', function(done){
       var entity = new Entity();
       assert(entity instanceof EventEmitter);
       done();
    });
    
    
    it('entityChangeState', function(done){
        
       var entity = new Entity(),
       mockObj = {
            handler : function(){}  
       },
       mock = sinon.mock(mockObj),
       arg = {};
       
        entity.changeState({
            events : [{
                name : 'testEvent',
                handler : function(obj){mockObj.handler(obj);}  
            }]
        });
       
       mock.expects('handler').withExactArgs(arg).once();
       entity.emit('testEvent', arg);
       
       mock.verify();
       done();
       
    });
    
    it('clearStateAfterDestroy', function(done){
        
       var entity = new Entity(),
       mockObj = {
            handler : function(){}  
       },
       mock = sinon.mock(mockObj),
        state = {
            events : [{
                name : 'testEvent',
                handler : function(){mockObj.handler();}
            }]
        };
       
        entity.changeState(state);
       chai.expect(entity.state).to.be.equal(state);
       entity.destroyEntity();
       
       mock.expects('handler').never();
       entity.emit('testEvent');
       
       chai.expect(entity.state).to.be.undefined;
       mock.verify();
       done();
       
    });
    
    it('entityChangeStateAndCleanPreviousState', function(done){
        
       var entity = new Entity(),
       mockObj = {
            handler1 : function(){},
            handler2 : function(){}
       },
       mock = sinon.mock(mockObj),
       arg1 = {},
       arg2 = {};
       
       mock.expects('handler1').withExactArgs(arg1).once();
       mock.expects('handler2').withExactArgs(arg2).once();
       
        entity.changeState({
            events : [{
                name : 'testEvent',
                handler : function(obj){mockObj.handler1(obj);}  
            }]
        });
       entity.emit('testEvent', arg1);
       
       entity.changeState({
            events : [{
                name : 'anotherTestEvent',
                handler : function(obj){mockObj.handler2(obj);}  
            }]
        });
       
       entity.emit('testEvent', arg1);
       entity.emit('anotherTestEvent', arg2);
       
       mock.verify();
       done();
       
    });
    
    it('destroyEntityScheduleEvent', function(done){
       var entity = new Entity(),
       spy = sinon.spy();
       
       entity.on('destroyEntity', spy);
       entity.destroyEntity();
       sinon.assert.calledOnce(spy);
       done();
    });
    
    
    it('destroyEntityScheduleEvent', function(done){
       var entity = new Entity(),
       spy = sinon.spy(),
       spy1 = sinon.spy();
       
       entity.on('destroyEntity', spy);
       entity.on('destroyEntityConnections', spy1);
       
       entity.destroyEntity();
       entity.destroyEntity();
       
       sinon.assert.calledOnce(spy);
       sinon.assert.calledOnce(spy1);
       done();
    });
    
    
    it('destroyEntityRecursive', function(done){
        
       var entity = new Entity(),
       child = new Entity(),
       spy = sinon.spy();
       
       entity.childEntity = child;
       child.parentEntity = entity;
       
       child.on('destroyEntity', function(){
            spy();
            chai.expect(child.parentEntity).to.be.instanceof(Entity);
            chai.expect(entity.childEntity).to.be.instanceof(Entity);
       });
       
       entity.destroyEntity();
       sinon.assert.calledOnce(spy);
       chai.expect(child.parentEntity).to.be.undefined;
       chai.expect(entity.childEntity).to.be.undefined;
       
       done();
    });
    
    
    it('destroyEntityWithInvalidChild', function(done){
        
       var entity = new Entity(),
       child = new EventEmitter(),
       spy = sinon.spy();
       
       entity.childEntity = child;
       child.parentEntity = entity;
       
       child.on('destroyEntity', spy);
       entity.destroyEntity();
       
       sinon.assert.neverCalledWith(spy);
       chai.expect(child.parentEntity).to.be.equal(entity);
       chai.expect(entity.childEntity).to.be.undefined;
       
       done();
    });
    
    
    it('sendEventForChilds', function(done){
       var entity = new Entity(),
       child = new Entity(),
       childMock = sandbox.mock(child);
       
       entity.childEntity = child;
       child.parentEntity = entity;
       
       childMock.expects('sendEventForChilds').withExactArgs('testEvt', 'arg1', 'arg2').once();
       entity.sendEventForChilds('testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    it('sendEventForChildsRecursively', function(done){
       var entity = new Entity(),
       testEventSpy1 = sinon.spy(),
       testEventSpy2 = sinon.spy();
       
       entity.on('testEvt2', function() {
           testEventSpy2();
       });
       
       entity.on('testEvt1', function(){
           testEventSpy1();
            entity.sendEventForChilds('testEvt1'); 
            entity.sendEventForChilds('testEvt2'); 
       });
       
       entity.sendEventForChilds('testEvt1');
       entity.sendEventForChilds('testEvt1');
       
       sinon.assert.calledTwice(testEventSpy1);
       sinon.assert.calledTwice(testEventSpy2);
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForParentsRecursively', function(done){
       var entity = new Entity(),
       testEventSpy1 = sinon.spy(),
       testEventSpy2 = sinon.spy();
       
       entity.on('testEvt2', function() {
           testEventSpy2();
       });
       
       entity.on('testEvt1', function(){
           testEventSpy1();
            entity.sendEventForParents('testEvt1'); 
            entity.sendEventForParents('testEvt2'); 
       });
       
       entity.sendEventForParents('testEvt1');
       entity.sendEventForParents('testEvt1');
       
       sinon.assert.calledTwice(testEventSpy1);
       sinon.assert.calledTwice(testEventSpy2);
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForChildsOrder', function(done){
       var entity = new Entity(),
       child = new Entity(),
       childSpy = sinon.spy(),
       parentSpy = sinon.spy();
       
       entity.childEntity = child;
       child.parentEntity = entity;
       
       child.on('testEvt', childSpy);
       entity.on('testEvt', parentSpy);
       
       entity.sendEventForChilds('testEvt', 'arg1', 'arg2');
       
       sinon.assert.calledOnce(childSpy);
       sinon.assert.calledOnce(parentSpy);
       sinon.assert.callOrder(childSpy, parentSpy);
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForChildsWithInvalidChild', function(done){
        
       var entity = new Entity(),
       child = new EventEmitter(),
       childMock = sandbox.mock(child);
       
       entity.childEntity = child;
       child.parentEntity = entity;
       child.sendEventForChilds = function(){};
       
       childMock.expects('sendEventForChilds').never();
       entity.sendEventForChilds('testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForParents', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('sendEventForParents').withExactArgs('testEvt', 'arg1', 'arg2').once();
       entity.sendEventForParents('testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForParentsOrder', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentSpy = sinon.spy(),
       childSpy = sinon.spy();
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       entity.on('testEvt', childSpy);
       parent.on('testEvt', parentSpy);
       
       entity.sendEventForParents('testEvt', 'arg1', 'arg2');
       
       sinon.assert.calledOnce(childSpy);
       sinon.assert.calledOnce(parentSpy);
       sinon.assert.callOrder(parentSpy, childSpy);
       
       sandbox.verify();
       done();
    });
    
    
    it('emitWhenSendEventForParentsWithoutMode', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent),
       entityMock = sandbox.mock(entity);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('emit').withExactArgs('testEvt', 'arg1', 'arg2').once();
       entityMock.expects('emit').withExactArgs('testEvt', 'arg1', 'arg2').once();
       entity.sendEventForParents('testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('emitWhenSendEventForParentsWithMode', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent),
       entityMock = sandbox.mock(entity);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('emit').withExactArgs('testEvt', 'arg1', 'arg2').once();
       entityMock.expects('emit').withExactArgs('testEvt', 'arg1', 'arg2').once();
       
       entity.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('noEmitWhenSendEventForParentsWithoutParams', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent),
       entityMock = sandbox.mock(entity);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('emit').never();
       entityMock.expects('emit').never();
       
       entity.sendEventForParents();
       
       sandbox.verify();
       done();
    });
    
    it('noEmitWhenSendEventForParentsWithOnlyMode', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent),
       entityMock = sandbox.mock(entity);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('emit').never();
       entityMock.expects('emit').never();
       
       entity.sendEventForParents(sessionMode.SERVER_ONLY);
       
       sandbox.verify();
       done();
    });
    
    
    it('correctArgsWhenSendEventForParentsWithMode', function(done){
        
       var entity = new Entity(),
       parent = new Entity(),
       parentMock = sandbox.mock(parent);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       
       parentMock.expects('sendEventForParents').withExactArgs(sessionMode.SERVER_ONLY, 'testEvt', 'arg1', 'arg2').once();
       entity.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('sendEventForParentsWithInvalidParent', function(done){
        
       var entity = new Entity(),
       parent = new EventEmitter(),
       parentMock = sandbox.mock(parent);
       
       entity.parentEntity = parent;
       parent.childEntity = entity;
       parent.sendEventForParents = function(){};
       
       parentMock.expects('sendEventForParents').never();
       entity.sendEventForParents('testEvt', 'arg1', 'arg2');
       
       sandbox.verify();
       done();
    });
    
    
    it('sessionInheritedFromEntity', function(done){
       var session = new Session(new EventEmitter());
       assert(session instanceof Entity);
       done();
    });
    
    it('socketDisconnectOnSessionDestroy', function(done){
       var socket = new EventEmitter(),
       socketMock,
       session;
       
       socket.disconnect = function(){};
       socketMock = sandbox.mock(socket);
       session = new Session(socket);
       
       chai.expect(session).to.have.property('socket', socket);
       socketMock.expects('disconnect').once();
       session.on('destroyEntity', function(){
            session.destroyEntity(); 
       });
       session.destroyEntity();
       session.destroyEntity();
       
       chai.expect(session).to.not.have.property('socket');
       sandbox.verify();
       done();
    });
    
    
    it('socketSendClientEventAfterDestroy', function(done){
       var socket = new EventEmitter(),
       session;
       
       socket.disconnect = function(){};
       session = new Session(socket);
       session.destroyEntity();
       
       session.sendClientEvent('invalidTestEvent');
       sandbox.verify();
       done();
    });
    
    
    it('destroyUserData', function(done){
        
       var info = {}, 
       user = new User({}, info);
       
       user.db = {};
       assert.equal(user.info, info);
       user.destroyEntity();
       
       chai.expect(user.info).to.be.undefined;
       chai.expect(user.db).to.be.undefined;
       done();
       
    });
    
    it('destroyUserWithAreaData', function(done){
        
       var info = {}, 
       area = {},
       user = new User({}, info),
       userWithArea = new UserWithArea(user, area);
       
       assert.equal(userWithArea.area, area);
       userWithArea.destroyEntity();
       
       chai.expect(userWithArea.area).to.be.undefined;
       done();
       
    });
    
    it('serverOnlyEventFromUserEntity', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('emit').withExactArgs('test', 'arg1', 'arg2').once();
        user.sendEventForParents(sessionMode.SERVER_ONLY, 'test', 'arg1', 'arg2');
        
        sandbox.verify();
        done();
    });
    
    
    it('singleClientEventFromUserEntity', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendClientEvent = function(){};
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('emit').withExactArgs('test', 'arg1', 'arg2').once();
        sessionMock.expects('sendClientEvent').withExactArgs('test', 'arg1', 'arg2').once();
        user.sendEventForParents(sessionMode.SINGLE_CLIENT, 'test', 'arg1', 'arg2');
        
        sandbox.verify();
        done();
    });
    
    
    it('broadcastClientsEventFromUserEntity', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendBroadcastClientEvent = function(){};
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('emit').withExactArgs('test', 'arg1', 'arg2').once();
        sessionMock.expects('sendBroadcastClientEvent').withExactArgs('test', 'arg1', 'arg2').once();
        user.sendEventForParents(sessionMode.BROADCAST_CLIENTS, 'test', 'arg1', 'arg2');
        
        sandbox.verify();
        done();
    });
    
    it('allClientsEventFromUserEntity', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendEventForAllClients = function(){};
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('emit').withExactArgs('test', 'arg1', 'arg2').once();
        sessionMock.expects('sendEventForAllClients').withExactArgs('test', 'arg1', 'arg2').once();
        user.sendEventForParents(sessionMode.ALL_CLIENTS, 'test', 'arg1', 'arg2');
        
        sandbox.verify();
        done();
    });
    
    it('noEmitWhenEmptyArgs', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('emit').never();
        user.sendEventForParents();
        
        sandbox.verify();
        done();
    });
    
    
    it('emitWhenNoSessionEventMode', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendBroadcastClientEvent = function(){};
        fakeSession.sendEventForAllClients = function(){};
        fakeSession.sendClientEvent = function(){};
        
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('sendBroadcastClientEvent').never();
        sessionMock.expects('sendClientEvent').never();
        sessionMock.expects('sendEventForAllClients').never();
        sessionMock.expects('emit').withExactArgs('testEvt', 'arg1', 'arg2').once();
        user.sendEventForParents('testEvt', 'arg1', 'arg2');
        
        sandbox.verify();
        done();
    });
    
    it('emitOnSessionEntityRecursivelyServerOnly', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        test1Spy = sinon.spy(),
        test2Spy = sinon.spy();
        
        fakeSession.on('testEvt2', test2Spy);
        
        fakeSession.on('testEvt1', function(){
            test1Spy();
            fakeSession.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt1', 'a', 'b');
            fakeSession.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt2', 'c', 'd'); 
        });
        
        fakeSession.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt1', 'a', 'b');
        fakeSession.sendEventForParents(sessionMode.SERVER_ONLY, 'testEvt1', 'a', 'b');
        
        sinon.assert.calledTwice(test1Spy);
        sinon.assert.calledTwice(test2Spy);
        
        sandbox.verify();
        done();
    });
    
    it('noEmitWhenNoParams', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendBroadcastClientEvent = function(){};
        fakeSession.sendEventForAllClients = function(){};
        fakeSession.sendClientEvent = function(){};
        
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('sendBroadcastClientEvent').never();
        sessionMock.expects('sendClientEvent').never();
        sessionMock.expects('sendEventForAllClients').never();
        sessionMock.expects('emit').never();
        user.sendEventForParents();
        
        sandbox.verify();
        done();
    });
    
    it('noEmitWhenOnlyMode', function(done){
        
        var fakeSession = new Session(new EventEmitter()),
        sessionMock,
        user;
        
        fakeSession.sendBroadcastClientEvent = function(){};
        fakeSession.sendEventForAllClients = function(){};
        fakeSession.sendClientEvent = function(){};
        
        sessionMock = sandbox.mock(fakeSession);
        user = new User(fakeSession);
        user.parentEntity = fakeSession;
        
        sessionMock.expects('sendBroadcastClientEvent').never();
        sessionMock.expects('sendClientEvent').never();
        sessionMock.expects('sendEventForAllClients').never();
        sessionMock.expects('emit').never();
        user.sendEventForParents(sessionMode.SINGLE_CLIENT);
        user.sendEventForParents(sessionMode.SERVER_ONLY);
        user.sendEventForParents(sessionMode.ALL_CLIENTS);
        user.sendEventForParents(sessionMode.BROADCAST_CLIENTS);
        
        sandbox.verify();
        done();
    });
    
    it('battleUserInheritedFromUserWithArea', function(done){
       var user = new User(new EventEmitter()),
       battleUser = new BattleUser(user, {}, {});
       assert(battleUser instanceof UserWithArea);
       done();
    });
    
    it('destroyBattleUserData', function(done){
        
       var info = {}, 
       area = {},
       user = new User({}, info),
       battleUser = new BattleUser(user, area);
       
       assert.equal(battleUser.area, area);
       battleUser.destroyEntity();
       
       chai.expect(battleUser.area).to.be.undefined;
       done();
       
    });
    
});

})();