(function(){
'use strict';

describe('protocol', function(){
    
    var decache = require("decache"),
    cfg,
    protocol,
    chai = require("chai");
    
    before(function(){
        
        decache('config');
        decache('protocol'); 
        
        cfg = require("config");
        cfg.messages = {
            "protocolTestEvent" : {
                "messageName" : "ProtocolTestMessage",
                "protoFile" : ['test', 'protocolTest.proto']
            },
            "anotherProtocolTestEvent" : {
                "messageName" : "ProtocolTestMessage",
                "protoFile" : ['test', 'protocolTest.proto']
            },
            "thirdProtocolTestEvent" : {
                "messageName" : "ProtocolTestMessage1",
                "protoFile" : ['test', 'protocolTest.proto']
            }
        };
        cfg.messages.__proto__ = {
           "invalidProtocolTestEvent" : {}
        };
        
        protocol = require("protocol");
    });
    
    after(function(){
       decache('config');
       decache('protocol'); 
    });
    
    it('getKeys', function(done){
        
        chai.expect(protocol.events)
            .that.is.an('array')
            .with.have.deep.property('length', 3);
        
        chai.expect(protocol.events)
            .have.deep.property('[0]', 'protocolTestEvent');
        
        done();
    });
   
    it('validateDecoding', function(done){
        
        chai.expect(function(){
            protocol.decode('invalidProtocolTestEvent');
        })
            .to.throw(Error)
            .have.deep.property('message').to.match(/.+invalidProtocolTestEvent/);

        done();
    });
    
    it('validateEncoding', function(done){
        
        chai.expect(function(){
            protocol.encode('invalidProtocolTestEvent');
        })
            .to.throw(Error)
            .have.deep.property('message').to.match(/.+invalidProtocolTestEvent/);

        done();
    });
    
   
    it('encodeAndDecode', function(done){
        
        var message = {
            testText1 : "ProtocolTestText1"
        },
        decodedMsg = protocol.decode(
            'protocolTestEvent', 
            protocol.encode('protocolTestEvent', message)
        ),
        anotherDecodedMessage = protocol.decode(
            'anotherProtocolTestEvent',
            protocol.encode('anotherProtocolTestEvent', message)
        ),
        thirdDecodedMessage = protocol.decode(
            'thirdProtocolTestEvent',
            protocol.encode('thirdProtocolTestEvent', message)
        );
        
        chai.expect(decodedMsg).to.have.property('testText2', 'defaultProtocolTestText2');
        chai.expect(decodedMsg).to.have.property('testText1', 'ProtocolTestText1');
        
        chai.expect(anotherDecodedMessage).to.have.property('testText2', 'defaultProtocolTestText2');
        chai.expect(anotherDecodedMessage).to.have.property('testText1', 'ProtocolTestText1');
        
        chai.expect(thirdDecodedMessage).to.have.property('testText2', 'defaultProtocolTestText123');
        chai.expect(thirdDecodedMessage).to.have.property('testText1', 'ProtocolTestText1');
        
        done();
    });
    
});

})();