(function initProtocol(){
'use strict';
var protobuf = require("protobufjs"),
path = require("path"),
cfg = require("config"),
messages = cfg.messages,
eventKeys = [],
util = require("util"),
InvalidEventException = function(evt){
    var self = this;
    self = Error.call(self, "Invalid event: "+evt);
    return self;
},
protocolMap = {},
buildersSet = {},
validate = function(evt)
{
    if(!protocolMap.hasOwnProperty(evt))
    {
        throw new InvalidEventException(evt);
    }
},
decode = function(evt, data){
    validate(evt);
    return protocolMap[evt].decode(data);
}, 
encode = function(evt, data){
    validate(evt);
    return protocolMap[evt].encode(data).toBuffer();
};

util.inherits(InvalidEventException, Error);
(function createProtoBuilders(){
    
    var prop,
    messageCfg,
    messageName,
    protoFilePath,
    currentDir = __dirname,
    joinArgs = [currentDir],
    protoBuilder;
    
    for(prop in messages){
        
        if(!messages.hasOwnProperty(prop)){
            continue;
        }
        
        eventKeys.push(prop);
        messageCfg = messages[prop];
        messageName = messageCfg['messageName'];
        
        protoFilePath = path.join.apply(path, joinArgs.concat(messageCfg['protoFile']));
        
        if(buildersSet.hasOwnProperty(messageName))
        {
            protocolMap[prop] = buildersSet[messageName];
            continue;
        }
        
        protoBuilder = protobuf.loadProtoFile(protoFilePath);
        protocolMap[prop] = buildersSet[messageName] = protoBuilder.build(messageName);
    }
    
})();

module.exports = {
    decode : decode,
    encode : encode,
    events : eventKeys
};

})();