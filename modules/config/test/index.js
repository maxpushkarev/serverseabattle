(function(){
'use strict';
var decache = require("decache"), 
cfg,
assert = require('assert');

    describe('config', function() {
        
        before(function(){
            decache("config");
            cfg = require("config"); 
        });
        
        after(function(){
            decache("config");
        });
        
        it('valid', function(done){
            assert(cfg instanceof Object);
            done();
        });
   
    });
    
})();