(function(){
'use strict';

describe('auth', function(){
    
    var decache = require("decache"),
    sinon = require("sinon"),
    assert = require("assert"),
    chai = require("chai"),
    sandbox = sinon.sandbox.create(),
    EventEmitter = require("events").EventEmitter,
    graph,
    auth,
    Session,
    NativeSession,
    NativeUser,
    cleanUp = function(){
        decache('fbgraph');
        decache('auth');
        decache('entity');
        decache('mongoose');
        decache('sbdata');
        sandbox.restore();
        Session = undefined;
    },
    prepareGraph = function(err, res){
        graph = require("fbgraph");
        sandbox.stub(graph, 'get', function(route, opts, callback){
            callback.apply(this, [err, res]);   
        });
    },
    prepareSBData = function(success, winsCount){
        var sbdata = require("sbdata");
        sandbox.stub(sbdata, 'getUser', function(id, handler, errHandler) {
            if(success)
            {
                handler.call(this, { wins : winsCount });
                handler.call(this, { wins : winsCount });
            }
            else
            {
                errHandler.call(this, 'Error');
                errHandler.call(this, 'Error');
            }
        });
    },
    prepareAuth = function(){
        auth = require("auth");    
    };
    
    beforeEach(function(){
        var entityAPI;
        
        cleanUp();
        
        entityAPI = require("entity");
        NativeSession = entityAPI.Session;
        NativeUser = entityAPI.User;
        
        sandbox.stub(entityAPI, 'Session', function(mockWrapper, fakeSocket){
            var self,
            nativeDestroyEntity;
            
            fakeSocket = fakeSocket || new EventEmitter();
            fakeSocket.disconnect = function(){};
            self = new NativeSession(fakeSocket);
            nativeDestroyEntity = self.destroyEntity;
            
            mockWrapper.mock = sandbox.mock(self);
            self.destroyEntity = function(){self.childEntity.destroyEntity()};
            self.nativeDestroyEntity = nativeDestroyEntity;
            
            return self;
        });
        
        Session = entityAPI.Session;
        
    });
    
    afterEach(function(){
        sandbox.restore();
        cleanUp();
    });
    
    it('sendAuthErrWhenErrorWhileRequestFB', function(done){
        
        var sessionMock = {},
        session = new Session(sessionMock);
        
        prepareGraph({}, undefined);
        prepareSBData(true, 123);
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('serverError').once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.undefined;
        done();
    });
    
    it('sendAuthErrWhenErrorWhileRequestMongo', function(done){
        
        var sessionMock = {},
        session = new Session(sessionMock);
        
        prepareSBData(false, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('serverError').once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.undefined;
        done();
    });
    
    it('sendAuthInfoWithPicture', function(done){
        var sessionMock = {},
        fakeSocket = new EventEmitter(),
        session = new Session(sessionMock, fakeSocket);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
    });
    
    
    it('authInfoAndUserCreatedOrder', function(done){
        var authInfoSpy = sinon.spy(),
        userCreatedForSessionSpy = sinon.spy(),
        session = new NativeSession(new EventEmitter());
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        
        sandbox.stub(session, 'sendClientEvent', function(){
            authInfoSpy();    
        });
        
        sandbox.stub(session, 'sendEventForChilds', function(){
            userCreatedForSessionSpy();    
        });
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sinon.assert.calledOnce(userCreatedForSessionSpy);
        sinon.assert.calledOnce(authInfoSpy);
        assert(authInfoSpy.calledAfter(userCreatedForSessionSpy));
        done();   
    });
    
    
    it('multipleAuthOnSession', function(done){
        var sessionMock = {},
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        session.emit('authMe', {
            value : ""
        });
        
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        done();   
    });
    
    
    it('multipleAuthOnSessionWhenBreakEntityConnections', function(done){
        var sessionMock = {},
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        delete(session.childEntity);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        done();   
    });
    
    
    it('autoKickOnDoubleEntrance', function(done){
        var mockWrapper = {},
        anotherMockWrapper = {},
        session = new Session(mockWrapper),
        anotherSession = new Session(anotherMockWrapper);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        mockWrapper.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        mockWrapper.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        mockWrapper.mock.expects('destroyEntity').once();
        
        anotherMockWrapper.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        anotherMockWrapper.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        auth.authorize(anotherSession);
        
        session.emit('authMe', {
            value : ""
        });
        
        anotherSession.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        done();
    });
    
    
    it('repeatedEntrance', function(done){
        var mockWrapper = {},
        anotherMockWrapper = {},
        session = new Session(mockWrapper),
        anotherSession = new Session(anotherMockWrapper);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : "qwerty"
                }
            }
        });
        prepareAuth();
        
        mockWrapper.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        mockWrapper.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        anotherMockWrapper.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ 
            id : "0",
            wins : 123,
            firstName : "Ivan",
            lastName : "Ivanov",
            picture : 'qwerty',
            link : "www.google.com"
        })).once();
        anotherMockWrapper.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        session.destroyEntity();
        
        auth.authorize(anotherSession);
        anotherSession.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        done();
    });
    
    
    
    it('sendAuthInfoWithoutPictureUrl', function(done){
        
        var mockWrapper = {},
        session = new Session(mockWrapper);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                }
            }
        });
        prepareAuth();
        
        mockWrapper.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ picture : undefined })).once();
        mockWrapper.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
           
    });
    
    
    it('sendAuthInfoWithObjectPictureUrl', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : {}
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ picture : undefined })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
           
    });
    
    it('sendAuthInfoWithEmptyPictureUrl', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
                data : {
                    url : ""
                }
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ picture : undefined })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
           
    });
    
    
    it('sendAuthInfoWithoutPictureData', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com",
            picture : {
            }
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ picture : undefined })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
           
    });
    
    it('sendAuthInfoWithoutPicture', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com"
        });
        prepareAuth();
        
        sessionMock.mock.expects('sendClientEvent').withArgs('authInfo', sinon.match({ picture : undefined })).once();
        sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').once();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session.childEntity).to.be.instanceof(NativeUser);
        chai.expect(session.childEntity.parentEntity).to.be.equal(session);
        done();   
          
    });
    
    
    it('skipWhenSessionDestroyedAfterRequestFB', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        graph = require("fbgraph");
        sandbox.stub(graph, 'get', function(route, opts, callback){
            
                session.nativeDestroyEntity();
                
                sessionMock.mock.expects('sendClientEvent').never();
                sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
                
                callback.apply(this, [null, {
                    id : 0,
                    first_name : "Ivan",
                    last_name : "Ivanov",
                    link : "www.google.com"
                }]);  
                
        });
        prepareAuth();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session).to.not.have.property('childEntity');
        done();   
          
    });
    
    
    it('skipWhenSessionDestroyedAfterErrorRequestFB', function(done){
        
        var sessionMock = {}, 
        session = new Session(sessionMock);
        
        prepareSBData(true, 123);
        graph = require("fbgraph");
        sandbox.stub(graph, 'get', function(route, opts, callback){
            
                session.nativeDestroyEntity();
                
                sessionMock.mock.expects('sendClientEvent').never();
                sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
                
                callback.apply(this, [{}, {
                    id : 0,
                    first_name : "Ivan",
                    last_name : "Ivanov",
                    link : "www.google.com"
                }]);
                
        });
        prepareAuth();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session).to.not.have.property('childEntity');
        done();   
          
    });
    
    
    it('skipWhenSessionDestroyedAfterRequestDB', function(done){
        
        var sessionMock = {},
        sbdata,
        session = new Session(sessionMock);
        
        sbdata = require("sbdata");
        sandbox.stub(sbdata, 'getUser', function(id, handler, errHandler) {
            
            session.nativeDestroyEntity();
            
            sessionMock.mock.expects('sendClientEvent').never();
            sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
                
            handler.call(this, { wins : 123 });
            handler.call(this, { wins : 123 });
        });
        
        
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com"
        });
        prepareAuth();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session).to.not.have.property('childEntity');
        done();   
          
    });
    
    
    it('skipWhenSessionDestroyedAfterErrorRequestDB', function(done){
        
        var sessionMock = {},
        sbdata,
        session = new Session(sessionMock);
        
        sbdata = require("sbdata");
        sandbox.stub(sbdata, 'getUser', function(id, handler, errHandler) {
            
            session.nativeDestroyEntity();
            
            sessionMock.mock.expects('sendClientEvent').never();
            sessionMock.mock.expects('sendEventForChilds').withArgs('userCreatedForSession').never();
            
            errHandler.call(this, 'Error');    
            errHandler.call(this, 'Error');
        });
        
        
        prepareGraph(null, {
            id : 0,
            first_name : "Ivan",
            last_name : "Ivanov",
            link : "www.google.com"
        });
        prepareAuth();
        
        auth.authorize(session);
        session.emit('authMe', {
            value : ""
        });
        
        sandbox.verify();
        chai.expect(session).to.not.have.property('childEntity');
        done();   
          
    });
    
    
});

})();