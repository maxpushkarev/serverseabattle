(function(){
'use strict';
var graph = require("fbgraph"),
entityAPI = require("entity"),
sessionMode = entityAPI.sessionEventMode,
sbdata  = require("sbdata"),
User = entityAPI.User,
authorizedUsers = {},
setPicture = function(res, info){
    
    var picture = res.picture,
        picData,
        picUrl;
    
    if(!picture) {
        return;
    }
    
    picData = picture.data;
    if(!picData) {
        return;
    }
    
    picUrl = picData.url;
    if(!picUrl) {
        return;
    }
    
    if(((typeof picUrl === 'string' || picUrl instanceof String) && !!picUrl)){
        info.picture = picUrl;     
    }
},
createUser = function(session, info, response, userdb){

    var newUser,
    currentUser,
    id = info.id;
    
    newUser = new User(session, info);
    session.childEntity = newUser;
    newUser.parentEntity = session;
    
    setPicture(response, info);
    newUser.db = userdb;
    info.wins = userdb.wins;
    
    currentUser = authorizedUsers[id];
    if(currentUser instanceof User) {
        var sessionForCurrentUser = currentUser.parentEntity;
        sessionForCurrentUser.destroyEntity();
    }
    
    authorizedUsers[id] = newUser;
    newUser.on('destroyEntity', function(){
        delete authorizedUsers[id];        
    });
    
    session.sendEventForChilds('userCreatedForSession');
    newUser.sendEventForParents(sessionMode.SINGLE_CLIENT, 'authInfo', info);
},
authorize = function(session){
    
    session.on('authMe', function(data){
        
        var token = data.textValue;
        session.removeAllListeners('authMe');
        
        session.on('createUserEvent', function(cfg) {
            session.removeAllListeners('createUserEvent');
            createUser(session, cfg.info, cfg.res, cfg.userdb);    
        });
        
        session.on('retreiveUserError', function(msg) {
            session.removeAllListeners('retreiveUserError');
            session.sendClientEvent('serverError', {
                textValue : msg
            });  
        });
        
        session.on('destroyEntity', function(){
            session.removeAllListeners('createUserEvent');
            session.removeAllListeners('retreiveUserError');
        });
        
        graph.get('me', {
            fields: "last_name, first_name, picture, link",
            locale : 'en_US',
            access_token : token
        }, function(err, res){
            
            var info,
            fbID;
            
            if(!!err) {
                session.emit('retreiveUserError', "Can't retreive user info(");
                return;
            }
            
            fbID = res.id;
            
            info = {
                
                id : fbID.toString(),
                firstName : res.first_name,
                lastName : res.last_name,
                link : res.link
                
            };
            
            sbdata.getUser(fbID, function(userdb){
                session.emit('createUserEvent', {
                    info : info,
                    res : res,
                    userdb : userdb
                });
            }, function(dbError){
                session.emit('retreiveUserError', "Can't retreive user from database");
            });
            
        });
    });
    
    
};

module.exports = {
    authorize : authorize 
};

})();