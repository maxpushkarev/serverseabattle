(function(){
    'use strict';
    var modules = require('./index.js'),
    path = require("path"),
    exec = require('child_process').exec;
    modules.run(function(directory, module){
        var linkPath = path.join(directory, module);
        exec('npm link '+linkPath);
    });
    
})();