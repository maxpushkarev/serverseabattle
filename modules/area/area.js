(function(){
'use strict';
var config = require("config"),
cfg = config.area,
cellStates = {
    IDLE : 0,
    SHIP : 1,
    FREE : 2,
    SHOT : 3
},
sideLength = cfg.sideLength,
maxCellIndex = sideLength - 1,
entityAPI = require("entity"),
UserWithArea = entityAPI.UserWithArea,
sessionMode = entityAPI.sessionEventMode,
createEmptyArea = function(){
    
    var i, j,
    area = {},
    rows = area.rows = [],
    row,
    cell;
    
    for(i = 0; i < sideLength; i++){
        
        row = [];
        rows.push(row);
        
        for(j = 0; j < sideLength; j++){
            
            cell = {
                state : cellStates.FREE
            };
            
            row.push(cell);
                        
        }    
    }
    
    
    area.cellShipCount = 0;
    return area;
},
validateIndexes = function(x, y){
    
    if(x < 0) {
        return false;
    }
    
    if(y < 0) {
        return false;
    }
    
    if(x > maxCellIndex) {
        return false;
    }
    
    if(y > maxCellIndex) {
        return false;
    }
    
    return true;
},
validateCellIndexes = function(cell){
    return validateIndexes(cell.x, cell.y);
},
checkCellPlacingRestrictions = function(area, cell){
    
    var cellInArea = area.rows[cell.x][cell.y];
    
    if(cellInArea.state !== cellStates.FREE) {
        return false;
    }
    
    return true;
    
},
checkRowSequence = function(cell, firstCell, index){
    return (cell.x === firstCell.x) && (cell.y === firstCell.y + index);
},
checkColumnSequence = function(cell, firstCell, index){
    return (cell.y === firstCell.y) && (cell.x === firstCell.x + index);
},
closeNeighbourCell = function(area, x, y){
    var cellInArea;
    if(!validateIndexes(x, y)){
        return;   
    }
    cellInArea = area.rows[x][y];
    if(cellInArea.state === cellStates.SHIP) {
        return;
    }
    cellInArea.state = cellStates.IDLE;
},
fillAreaWithShip = function(area, template, ship, shipsCfg, checkSequence){
    
    var cells = ship.cells,
    cellsCount = cells.length,
    i,
    cell,
    cellInArea,
    x,
    y,
    firstCell;
    
    if(cellsCount !== shipsCfg.size) {
        return false;
    }
    
    firstCell = cells[0];
    for(i = 0; i < cellsCount; i++) {
        
        cell = cells[i];
        
        if(!validateCellIndexes(cell)) {
            return false;    
        }
        
        if(!checkCellPlacingRestrictions(area, cell)) {
            return false;    
        }
        
        if(!checkSequence(cell, firstCell, i)) {
            return false;
        }
    }
    
    for(i = 0; i < cellsCount; i++) {
        
        cell = cells[i];
        x = cell.x;
        y = cell.y;
        cellInArea = area.rows[x][y];
        
        cellInArea.state = cellStates.SHIP;
        area.cellShipCount++;
        
        closeNeighbourCell(area, x - 1, y);
        closeNeighbourCell(area, x + 1, y);
        closeNeighbourCell(area, x, y - 1);
        closeNeighbourCell(area, x, y + 1);
        closeNeighbourCell(area, x - 1, y - 1);
        closeNeighbourCell(area, x + 1, y + 1);
        closeNeighbourCell(area, x - 1, y + 1);
        closeNeighbourCell(area, x + 1, y - 1);
    }
    
    return true;
    
},
fillAreaWithShips = function(area, template, prop){
    
    var shipsCollection = template[prop],
    shipsCountInTemplate = shipsCollection.length,
    shipsCfg = cfg[prop],
    i,
    ship;
    
    if(shipsCountInTemplate !== shipsCfg.count) {
        return false;
    }
    
    for(i = 0; i < shipsCountInTemplate; i++) {
        ship = shipsCollection[i];
        if(!(
                fillAreaWithShip(area, template, ship, shipsCfg, checkRowSequence) || 
                fillAreaWithShip(area, template, ship, shipsCfg, checkColumnSequence)
            )
        ) {
            return false;
        }
    }
        
    return true;
},
fillAreaWithTemplate = function(area, template){
    
    if(!fillAreaWithShips(area, template, "ships4")) {
        return false;
    }   
    
    if(!fillAreaWithShips(area, template, "ships3")) {
        return false;
    }
    
    if(!fillAreaWithShips(area, template, "ships2")) {
        return false;
    }
    
    if(!fillAreaWithShips(area, template, "ships1")) {
        return false;
    }
    
    return true;
},
finalizeAreaBuildingWithError = function(user, msg){
    user.removeAllListeners('requestForSelfArea');
    user.sendEventForParents(sessionMode.SINGLE_CLIENT, 'serverError', {
        textValue : msg
    });    
},
configureUserForAreaBuilding = function(user){
    
    user.on('requestForSelfArea', function(areaTemplate){
    
        var area,
        userWithArea;
    
        if(user.childEntity instanceof UserWithArea) {
            finalizeAreaBuildingWithError(user, "User already have area");
            return;
        }
        
        area = createEmptyArea();
        
        if(!fillAreaWithTemplate(area, areaTemplate)) {
            finalizeAreaBuildingWithError(user, "Invalid user area template");       
        }
        else
        {
            userWithArea = new UserWithArea(user, area);
            userWithArea.parentEntity = user;
            user.childEntity = userWithArea;
            
            user.parentEntity.sendEventForChilds('areaCreatedForUser');
        }
        
    });
    
};

module.exports = {
    configureUserForAreaBuilding : configureUserForAreaBuilding,
    cellStates : cellStates
};

})();