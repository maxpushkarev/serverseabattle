(function(){
'use strict';
describe('area', function(){
    var config = require("config"),
    decache = require("decache"),
    sinon = require("sinon"),
    chai = require("chai"),
    sandbox = sinon.sandbox.create(),
    userMock,
    sessionMode,
    NativeUser,
    EventEmitter = require("events").EventEmitter,
    User,
    Session,
    UserWithArea,
    BattleUser,
    areaAPI,
    cleanUp = function(){
        sandbox.restore();
        decache('entity');
        decache('area');
        NativeUser = undefined;
    };
    
    beforeEach(function(){
        
        var entities;
        
        cleanUp();
        entities = require("entity");
        NativeUser = entities.User;
        Session = entities.Session;
        sessionMode = entities.sessionEventMode;
        UserWithArea = entities.UserWithArea;
        
        sandbox.stub(entities, 'User', function(){
            var self,
            session;
            self = new NativeUser();
            session = new Session(new EventEmitter());
            userMock = sandbox.mock(self);
            self.parentEntity = session;
            session.childEntity = self;
            return self;
        });
        
        BattleUser = entities.BattleUser;
        User = entities.User;
        areaAPI = require("area");
    });
    
    afterEach(function(){
        cleanUp();
    });
    
    it('invalidShips4withNegativeCoord', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:-1, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidShips3withOverflowCoord', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:config.area.sideLength}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidShips2withNegativeCoord', function(done){
       
         var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:-8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidShips1withOverflowCoord', function(done){
       
         var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5+config.area.sideLength}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidCountOfShips', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidSequenceOfCellsInShip', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:5, y:4}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('spaceInShip', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:7, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidOrderInShip', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:5, y:0}, {x:4, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('invalidCountOfCellsInShip', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    
    it('cellConflictInShips', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:3, y:0}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('cellTouchInShips', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:3, y:1}, {x:3, y:2}, {x:3, y:3}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('cellAngleRestrictionBetweenShips', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:2, y:1}, {x:2, y:2}, {x:2, y:3}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('multipleCreateArea', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').withExactArgs('areaCreatedForUser').once();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
        user.emit('requestForSelfArea', template);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.instanceof(UserWithArea);
        chai.expect(user.childEntity.parentEntity).to.be.equal(user);
        done();
    });
    
    it('createAreaWhenBattleUserExist', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        user.childEntity = new BattleUser();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.instanceof(BattleUser);
        done();
    });
    
    
    it('multipleRequestInvalidArea', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:-9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
        user.emit('requestForSelfArea', template);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('blockAreaBuildingAfterInvalidRequest', function(done){
       
        var validTemplate = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        invalidTemplate = {
            ships4 : [{
                cells : [{x:3, y:-8}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').never();
        userMock.expects('sendEventForParents').withArgs(sessionMode.SINGLE_CLIENT, 'serverError').once();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', invalidTemplate);
        user.emit('requestForSelfArea', validTemplate);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.undefined;
        done();
    });
    
    
    it('createUserWithArea', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        userMock.expects('sendEventForChilds').withExactArgs('areaCreatedForUser').once();
        userMock.expects('sendEventForParents').never();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity).to.be.instanceof(UserWithArea);
        chai.expect(user.childEntity.parentEntity).to.be.equal(user);
        done();
    });
    
    
    it('validCellShipCount', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        areaCfg = config.area,
        user = new User();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity.area).to.have.property('cellShipCount').equal(
            areaCfg.ships1.count * areaCfg.ships1.size +
            areaCfg.ships2.count * areaCfg.ships2.size +
            areaCfg.ships3.count * areaCfg.ships3.size +
            areaCfg.ships4.count * areaCfg.ships4.size
        );
        done();
    });
    
    
    it('shipCellNotClosing', function(done){
       
        var template = {
            ships4 : [{
                cells : [{x:3, y:0}, {x:4, y:0}, {x:5, y:0}, {x:6, y:0}]
            }],
            ships3 : [{
                cells : [{x:0, y:3}, {x:0, y:4}, {x:0, y:5}]
            },{
                cells : [{x:5, y:3}, {x:6, y:3}, {x:7, y:3}]
            }],
            ships2 : [{
                cells : [{x:3, y:5}, {x:3, y:6}]
            },{
                cells : [{x:1, y:8}, {x:1, y:9}]
            },{
                cells : [{x:7, y:6}, {x:7, y:7}]
            }],
            ships1 : [{
                cells : [{x:9, y:5}]
            },{
                cells : [{x:8, y:1}]
            },{
                cells : [{x:5, y:5}]
            },{
                cells : [{x:3, y:8}]
            }]
        },
        user = new User();
        
        areaAPI.configureUserForAreaBuilding(user);
        user.emit('requestForSelfArea', template);
       
        sandbox.verify();
        chai.expect(user.childEntity.area.rows[4][0]).to.have.property('state').equal(areaAPI.cellStates.SHIP);
        done();
    });
    
});

})();