(function(){
'use strict';
describe('player', function(){
    
    var decache = require("decache"),
    sinon = require("sinon"),
    EventEmitter = require("events").EventEmitter,
    entities = require("entity"),
    Session = entities.Session,
    User = entities.User,
    UserWithArea = entities.UserWithArea,
    sandbox = sinon.sandbox.create(),
    cleanUp = function(){
        decache('player');
        decache('auth');
        decache('area');
        decache('userlist');
        decache('battle');
    },
    auth,
    area,
    battle,
    userlist,
    authFake = {
        authorize : function(){}
    },
    areaFake = {
        createAreaIfPossible : function(){}
    },
    userListFake = {
        attachToList: function(){}  
    },
    battleFake = {
        createBattle : function(){}  
    },
    authSpy,
    areaSpy,
    userlistSpy,
    battleSpy,
    user,
    userWithArea,
    player,
    prepareLibs = function(cfg){
        
        sandbox.stub(auth, 'authorize', function(session){
            
            authFake.authorize(session);
            
            if(!cfg.auth) {
                return;
            }
            
            user = new User(session, {});
            user.parentEntity = session;
            session.childEntity = user;
            session.sendEventForChilds('userCreatedForSession');
        });
        
        sandbox.stub(area, 'configureUserForAreaBuilding', function(user){
            areaFake.createAreaIfPossible(user);
            userWithArea = new UserWithArea();
            userWithArea.parentEntity = user;
            user.childEntity = userWithArea;
            user.parentEntity.sendEventForChilds('areaCreatedForUser');
        });
        
        sandbox.stub(userlist, 'attach', function(userWithArea){
            userListFake.attachToList(userWithArea);
        });
        
        sandbox.stub(battle, 'createBattle', function(inviter, invited){
            battleFake.createBattle(inviter, invited);
        });
        
        player = require("player");    
    };
    
    beforeEach(function() {
        cleanUp();
        
        battleSpy = sinon.spy(battleFake, 'createBattle');
        authSpy = sinon.spy(authFake, 'authorize');
        areaSpy = sinon.spy(areaFake, 'createAreaIfPossible');
        userlistSpy = sinon.spy(userListFake, 'attachToList');
        
        battle = require("battle");
        auth = require("auth");
        area = require("area");
        userlist = require("userlist");
    });
    
    afterEach(function(){
        
        battleFake.createBattle.restore();
        authFake.authorize.restore(); 
        areaFake.createAreaIfPossible.restore(); 
        userListFake.attachToList.restore(); 
        
        sandbox.restore();
        cleanUp();
    });
    
    it('prepareWithInvalidSession', function(done){
        
        prepareLibs({
            auth : true
        });
        
        player.prepare();
        
        sinon.assert.neverCalledWith(areaSpy);
        sinon.assert.neverCalledWith(authSpy);
        sandbox.verify();
        
        done();
    });
    
    it('authorizeWhenValidSession', function(done){
        
        var session = new Session(new EventEmitter());
        
        prepareLibs({
            auth : true
        });

        player.prepare(session);
        
        sinon.assert.calledWithExactly(areaSpy, user);
        sinon.assert.calledWithExactly(authSpy, session);
        sinon.assert.calledOnce(authSpy);
        sandbox.verify();
        
        done();
    });
    
    it('configureAreaOnAuthorizedUser', function(done){
        
        var session = new Session(new EventEmitter());
        
        prepareLibs({
            auth : true
        });
        
        player.prepare(session);
        
        
        sinon.assert.calledWithExactly(areaSpy, user);
        sinon.assert.calledOnce(areaSpy);
        sinon.assert.calledWithExactly(authSpy, session);
        sinon.assert.calledOnce(authSpy);
        
        sandbox.verify();
        
        done();
    });
    
    
    it('dontConfigureAreaOnUnauthorizedUser', function(done){
        
        var session = new Session(new EventEmitter());
        
        prepareLibs({
            auth : false
        });
        
        player.prepare(session);
        
        sinon.assert.neverCalledWith(areaSpy);
        sinon.assert.calledWithExactly(authSpy, session);
        sinon.assert.calledOnce(authSpy);
        sandbox.verify();
        
        done();
    });
    
    
    it('attachUserWithAreaToList', function(done){
        
        var session = new Session(new EventEmitter());
        
        prepareLibs({
            auth : true
        });
        
        player.prepare(session);
        
        
        sinon.assert.calledWithExactly(userlistSpy, userWithArea);
        sandbox.verify();
        
        done();
    });
    
    
    it('createBattleOnEventHandling', function(done){
        
        var session = new Session(new EventEmitter()),
        inviterUserWithArea = new UserWithArea();
        
        prepareLibs({
            auth : true
        });
        
        player.prepare(session);
        session.emit('createBattleWithInviter', inviterUserWithArea);
        
        sinon.assert.calledWithExactly(battleSpy,  inviterUserWithArea, userWithArea);
        sandbox.verify();
        done();
    });
    
    
});
    
})();