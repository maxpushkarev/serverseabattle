(function(){
'use strict';
    var auth = require("auth"),
    areaAPI = require("area"),
    entityAPI = require("entity"),
    userlist = require("userlist"),
    battle = require("battle"),
    Session = entityAPI.Session,
    prepare = function(session){
        
        if(!(session instanceof Session)) {
            return;
        }
        
        session.on('createBattleWithInviter', function(inviter) {
            battle.createBattle(inviter, session.childEntity.childEntity);  
        });
        
        session.on('userCreatedForSession', function(){
            areaAPI.configureUserForAreaBuilding(session.childEntity);
        });
        
        session.on('areaCreatedForUser', function(){
            userlist.attach(session.childEntity.childEntity);        
        });
        
        auth.authorize(session);    
    };
    
    module.exports = {
        prepare : prepare  
    };
    
})();