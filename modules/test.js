(function(){
    'use strict';
    var modules = require('./index.js'),
    path = require("path"),
    exec = require('child_process').exec,
    testOrdered = [],
    testModuleIndex;
    
    modules.run(function(directory, module){
        
           var moduleTestExecutor = function(){
           
                var wd = path.join(directory, module),
                p;
                
                p = exec('npm test', {
                    cwd : wd
                });
                
                p.stdout.on('data', function(data){
                    process.stdout.write(data);
                });
                
                p.stderr.on('data', function(data){
                    process.stderr.write(data);
                });
        
                p.on('exit', function(code){
                    
                    if(code == 0)
                    {
                        testModuleIndex++;
                        
                        if(testModuleIndex <  testOrdered.length)
                        {
                            testOrdered[testModuleIndex].call(this);    
                        }
                        
                        return;
                    }
                    
                    throw new Error("Failed! Module : "+module);
                });
               
           };
           
           testOrdered.push(moduleTestExecutor);

    }, function(){
        testModuleIndex = 0;
        testOrdered[testModuleIndex].call(this);
    });
    
})();