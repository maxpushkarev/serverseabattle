(function warmUpModules(){
'use strict';
    var modules = require('./modules/index.js');
    modules.run(function(directory, module){
        (function(m){
            require(m);
        })(module);
    });
    console.log('Server started!!!');
})();